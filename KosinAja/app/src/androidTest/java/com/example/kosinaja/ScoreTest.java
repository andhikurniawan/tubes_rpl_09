package com.example.kosinaja;

import androidx.test.rule.ActivityTestRule;

import org.junit.Rule;
import org.junit.Test;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.replaceText;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.matcher.ViewMatchers.withId;

public class ScoreTest {
    @Rule
    public ActivityTestRule<score> activityTestRule = new ActivityTestRule<>(score.class);

    private String scoreTest = "90";

    @Test
    public void btn() {
        onView(withId(R.id.review_next)).perform(click());
    }

    @Test
    public void scoreEditable() {
        onView(withId(R.id.scoreText)).perform(typeText(scoreTest),closeSoftKeyboard());
    }


}
