package com.example.kosinaja;

import org.junit.After;
import org.junit.Before;

import androidx.test.rule.ActivityTestRule;
import org.junit.Rule;
import org.junit.Test;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.replaceText;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.hasErrorText;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static org.junit.Assert.*;
public class ProfileActivityTest {

    @Rule
    public  ActivityTestRule<ProfileActivity> sherly =  new ActivityTestRule<>(ProfileActivity.class);

    private String emailsherly = "test@gmail.com";
    private String passwordsherly = "1234haii";
    private String user = "sherly";
    private String bio = "Haloo semua";
    private String newpass = "1234567";


    @Test
    public void InputValidEditableInsert(){
        onView(withId(R.id.bio)).perform(replaceText(bio), closeSoftKeyboard());
        onView(withId(R.id.nameprof)).perform(replaceText(user), closeSoftKeyboard());
        onView(withId(R.id.emailprof)).perform(replaceText(emailsherly), closeSoftKeyboard());
        onView(withId(R.id.passwordprof)).perform(replaceText(passwordsherly), closeSoftKeyboard());
        onView(withId(R.id.newpass)).perform(replaceText(newpass), closeSoftKeyboard());
    }

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }
}