package com.example.kosinaja;

import androidx.test.rule.ActivityTestRule;

import org.junit.Rule;
import org.junit.Test;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.replaceText;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.matcher.ViewMatchers.withId;

public class LoginActivityTest {
    @Rule
    public ActivityTestRule<MainActivity> activityTestRule = new ActivityTestRule<>(MainActivity.class);

    private String email = "test";
    private String password = "test12345";

    @Test
    public void emailEditable() {
        onView(withId(R.id.emaillog)).perform(typeText(email),closeSoftKeyboard());
    }

    @Test
    public void passwordEditable() {
        onView(withId(R.id.passwordlog)).perform(typeText(password),closeSoftKeyboard());
    }
}
