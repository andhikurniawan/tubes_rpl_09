package com.example.kosinaja;

import android.app.Instrumentation;

import androidx.test.rule.ActivityTestRule;

import org.junit.Rule;
import org.junit.Test;

import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.replaceText;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.matcher.ViewMatchers.withId;

public class LoginActivity {

    @Rule
    public ActivityTestRule<MainActivity> activityTestRule = new ActivityTestRule<>(MainActivity.class);

    private String email = "test";
    private String password = "Test123";

    @Test
    public void emailEditable() {
        onView(withId(R.id.emaillog)).perform(typeText(email), closeSoftKeyboard());

    }

    @Test
    public void passwordEditable() {
        onView(withId(R.id.passwordlog)).perform(typeText(password), closeSoftKeyboard());

    }
    @Test
    public void buttonClicked(){
        onView(withId(R.id.btnLogin)).perform(click());
    }
    @Test
    public void validateInput(){
        onView(withId(R.id.emaillog)).perform(replaceText(email), closeSoftKeyboard());
        onView(withId(R.id.btnLogin)).perform(click());
        onView(withId(R.id.passwordlog)).perform(replaceText(email), closeSoftKeyboard());
    }
}
