package com.example.kosinaja;

import androidx.test.espresso.ViewAction;
import androidx.test.espresso.matcher.ViewMatchers;
import androidx.test.rule.ActivityTestRule;

import org.junit.Rule;
import org.junit.Test;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.replaceText;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.matcher.ViewMatchers.hasErrorText;
import static androidx.test.espresso.matcher.ViewMatchers.isChecked;
import static androidx.test.espresso.matcher.ViewMatchers.isSelected;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static java.util.regex.Pattern.matches;
import static org.hamcrest.core.IsNot.not;

public class PesanKost1ActivityTest {
    @Rule
    public ActivityTestRule<pesankost1> activityTestRule = new ActivityTestRule<>(pesankost1.class);

    private String nama = "nama";
    private String nohp = "0812345";
    private String pekerjaan = "mahasiswa";
    private String namauniv = "test university";


    @Test
    public void validasi() {
        onView(withId(R.id.nama_input)).perform(typeText(nama),closeSoftKeyboard());
        onView(withId(R.id.nohp_input)).perform(typeText(nohp),closeSoftKeyboard());
        onView(withId(R.id.pekerjaan_input)).perform(typeText(pekerjaan),closeSoftKeyboard());
        onView(withId(R.id.next_btn)).perform(click());
    }


    @Test
    public void namaEditable() {
        onView(withId(R.id.nama_input)).perform(typeText(nama),closeSoftKeyboard());
    }

    @Test
    public void nohpEditable() {
        onView(withId(R.id.nohp_input)).perform(typeText(nohp),closeSoftKeyboard());
    }

    @Test
    public void pekerjaanEditable() {
        onView(withId(R.id.pekerjaan_input)).perform(typeText(pekerjaan),closeSoftKeyboard());
    }

    @Test
    public void namaunivEditable() {
        onView(withId(R.id.namauniv_input)).perform(typeText(namauniv),closeSoftKeyboard());
    }

    @Test
    public void nextButton() {
        onView(withId(R.id.next_btn)).perform(click());
    }

}
