package com.example.kosinaja;

import org.junit.After;
import org.junit.Before;

import androidx.test.rule.ActivityTestRule;
import org.junit.Rule;
import org.junit.Test;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.replaceText;
import static androidx.test.espresso.matcher.ViewMatchers.withId;

public class RegisterActivityTest {

    @Rule
    public  ActivityTestRule<RegisterActivity> sherly =  new ActivityTestRule<>(RegisterActivity.class);

    private String user = "sherly";
    private String emailsherly = "test@gmail.com";
    private String passwordsherly = "1234haii";


    @Test
    public void InputValidRegister(){

        onView(withId(R.id.namereg)).perform(replaceText(user), closeSoftKeyboard());
        onView(withId(R.id.emaillog)).perform(replaceText(emailsherly), closeSoftKeyboard());
        onView(withId(R.id.passwordlog)).perform(replaceText(passwordsherly), closeSoftKeyboard());
        onView(withId(R.id.btnregist)).perform(click());

    }

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }
}