package com.example.kosinaja;

import androidx.test.rule.ActivityTestRule;

import org.junit.Rule;
import org.junit.Test;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.replaceText;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.matcher.ViewMatchers.withId;

public class RegistryActivityTest {

    @Rule
    public ActivityTestRule<RegisterActivity> activityTestRule = new ActivityTestRule<>(RegisterActivity.class);

    private String name = "testing";
    private String email = "test123";
    private String password = "test12345";
    private String confirm = "test123";

    @Test
    public void nameEditable() {
        onView(withId(R.id.namereg)).perform(typeText(name),closeSoftKeyboard());
    }
    @Test
    public void emailEditable() {
        onView(withId(R.id.emaillog)).perform(typeText(email),closeSoftKeyboard());
    }

    @Test
    public void passwordEditable() {
        onView(withId(R.id.passwordlog)).perform(typeText(password),closeSoftKeyboard());
    }
    @Test
    public void confirmEditable() {
        onView(withId(R.id.cpassreg)).perform(typeText(confirm),closeSoftKeyboard());
    }
}