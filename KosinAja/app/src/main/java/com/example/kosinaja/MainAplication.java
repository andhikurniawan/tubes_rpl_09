package com.example.kosinaja;

import android.app.Application;
import android.content.Context;

import com.example.kosinaja.Helper.LocaleHelper;

public class MainAplication extends Application {
    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(LocaleHelper.OnAttach(base,"en"));
    }
}


