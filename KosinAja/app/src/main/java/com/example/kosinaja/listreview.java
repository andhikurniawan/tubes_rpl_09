package com.example.kosinaja;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class listreview extends AppCompatActivity {
    public String scoreText_string;
    public String isikomentar_string;
    public String ratingBar_string;
    public String ratingBar_string2;
    public String ratingBar_string3;

    Button masukreview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listreview);

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference("reviewkost");

        Intent i = getIntent();
        scoreText_string = i.getStringExtra("scoreText_string");
        isikomentar_string = i.getStringExtra("isikomentar");
        /*
        ratingBar_string = i.getStringExtra("kebersihan");
        ratingBar_string2 = i.getStringExtra("keamanan");
        ratingBar_string3 = i.getStringExtra("fasilitas");

         */

        TextView hasilscore = (TextView)findViewById(R.id.hasilscore);
        hasilscore.setText(String.valueOf(scoreText_string));
        TextView hasilreview = (TextView)findViewById(R.id.hasilreview);
        hasilreview.setText(String.valueOf(isikomentar_string));
/*
        TextView kebersihan = (TextView)findViewById(R.id.isikebersihan);
        kebersihan.setText(String.valueOf(ratingBar_string));
        TextView kemanan = (TextView)findViewById(R.id.isikeamanan);
        kemanan.setText(String.valueOf(ratingBar_string2));
        TextView fasilitas = (TextView)findViewById(R.id.isifasilitas);
        fasilitas.setText(String.valueOf(ratingBar_string3));

 */

        masukreview = findViewById(R.id.hasilsubmit);
        masukreview.setPaintFlags(Paint.UNDERLINE_TEXT_FLAG);

        masukreview.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick (View v){
                Intent i = new Intent(listreview.this, hasilreview.class);
                startActivity(i);
            }
        });
    }
}
