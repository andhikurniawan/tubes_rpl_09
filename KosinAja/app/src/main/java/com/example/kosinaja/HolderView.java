package com.example.kosinaja;

import android.content.Context;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

public class HolderView extends RecyclerView.ViewHolder {

    private View mView;
    private ClickListener mClickListener;
    Button btn_detail;
    TextView mTitle, mDesc, mHarga, mStok, mGender, mAlamat, mLokasi, mWaktu, mLatitude, mLongitude;
    ImageView mImageView;

    public HolderView(@NonNull View itemView) {
        super(itemView);

        mView=itemView;
        btn_detail=(Button)mView.findViewById(R.id.Btn_Detail);
        mTitle = mView.findViewById(R.id.rTitle);
        mDesc = mView.findViewById(R.id.rDescription);
        mHarga = mView.findViewById(R.id.rHarga);
        mStok = mView.findViewById(R.id.rStok);
        mGender = mView.findViewById(R.id.rGender);
        mAlamat = mView.findViewById(R.id.rAlamat);
        mLokasi = mView.findViewById(R.id.rLokasi);
        mWaktu = mView.findViewById(R.id.rWaktu);
        mImageView = mView.findViewById(R.id.rImage);
        mLatitude=mView.findViewById(R.id.latitude);
        mLongitude=mView.findViewById(R.id.longitude);

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mClickListener.onItemClick(view, getAdapterPosition());
            }
        });

        itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                mClickListener.onItemLongClick(view, getAdapterPosition());
                return true;
            }
        });
    }

    public void setDetails(Context ctx, String title, String image, String description, String harga, String gender, String stok, String alamat, String lokasi, String waktu, String latitude, String longitude){
        TextView mTitle = mView.findViewById(R.id.rTitle);
        ImageView mImage = mView.findViewById(R.id.rImage);
        TextView mDesc = mView.findViewById(R.id.rDescription);
        TextView mHarga = mView.findViewById(R.id.rHarga);
        TextView mStok = mView.findViewById(R.id.rStok);
        TextView mAlamat = mView.findViewById(R.id.rAlamat);
        TextView mGender= mView.findViewById(R.id.rGender);
        TextView mLokasi = mView.findViewById(R.id.rLokasi);
        TextView mWaktu = mView.findViewById(R.id.rWaktu);
        TextView mLatitude=mView.findViewById(R.id.latitude);
        TextView mLongitude=mView.findViewById(R.id.longitude);


        mTitle.setText(title);
        mDesc.setText(description);
        mHarga.setText("Rp."+harga);
        mStok.setText("Ada " + stok);
        mGender.setText(gender);
        mAlamat.setText(alamat);
        mLokasi.setText(lokasi);
        mWaktu.setText("/" +waktu);
        mLatitude.setText(latitude);
        mLongitude.setText(longitude);
        Picasso.get().load(image).into(mImage);

    }


    public interface ClickListener{
        void onItemClick(View view, int position);
        void onItemLongClick(View view, int position);
    }
    public void setOnClickListener(HolderView.ClickListener clickListener){
        this.mClickListener = clickListener;

    }
}
