package com.example.kosinaja;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;

public class PostDetailActivity extends AppCompatActivity implements View.OnClickListener {

    RecyclerView commentRV;
    CommentAdapter commentAdapter;
    ArrayList<ModelComment> modelCommentArrayList = new ArrayList<>();
    DatabaseReference db ;
    EditText etComment;
    Button btnComment;
    FirebaseAuth mAuth;
    String id;
    public String comment_et;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_detail);

        mAuth = FirebaseAuth.getInstance();

        //declare xml components
        final TextView tv_name = findViewById(R.id.tv_name);
        final TextView tv_status = findViewById(R.id.tv_status);
        final ImageView postpic = findViewById(R.id.imgView_postPic);
        ImageView avatar = findViewById(R.id.imgView_proPic);
        etComment = findViewById(R.id.et_comment);
        btnComment = findViewById(R.id.btn_comment);
        comment_et = etComment.getText().toString();

        btnComment.setOnClickListener(this);
        Intent data = getIntent();
        id = data.getStringExtra("id");

        db = FirebaseDatabase.getInstance().getReference("fjbPosts");

        Query query = db.orderByChild("id").equalTo(id);

        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                for (DataSnapshot ds : dataSnapshot.getChildren()){

                    String email = ds.child("email").getValue().toString();
                    String post = ds.child("post").getValue().toString();
                    String image = ds.child("image").getValue().toString();

                    tv_name.setText(email);
                    tv_status.setText(post);
                    Glide.with(PostDetailActivity.this).load(image).into(postpic);
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


        commentRV = (RecyclerView) findViewById(R.id.rowcomment);
        //RecyclerView.LayoutManager layoutManager = ;
        commentRV.setLayoutManager(new LinearLayoutManager(this));
        commentAdapter = new CommentAdapter(this, modelCommentArrayList);
        commentRV.setAdapter(commentAdapter);

        db.child(id).child("Comments").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                modelCommentArrayList.clear();
                for (DataSnapshot ds : dataSnapshot.getChildren()){
                    String id = ds.child("id").getValue().toString();
                    String email = ds.child("email").getValue().toString();
                    String comment = ds.child("comment").getValue().toString();

                    ModelComment mc = new ModelComment(id,email,comment);
                    modelCommentArrayList.add(mc);
                }
                commentAdapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    @Override
    public void onClick(View v) {
        FirebaseUser user = mAuth.getCurrentUser();
        String comment = etComment.getText().toString();
        String idc = db.push().getKey();

        HashMap<Object,String> data = new HashMap<>();
        data.put("id",idc);
        data.put("email",user.getEmail());
        data.put("comment",comment);

        db.child(id).child("Comments").child(idc).setValue(data);
            Toast.makeText(this, "Berhasil komentar", Toast.LENGTH_SHORT).show();

    }
}
