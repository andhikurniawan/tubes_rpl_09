package com.example.kosinaja;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;

public class ProfileActivity extends AppCompatActivity {

    EditText user,emailprofile,pass,confirm,biouser;
    FirebaseDatabase database;
    DatabaseReference myref;
    ImageView upload;

    String PROFILE_IMAGE_URL = null;
    int TAKE_IMAGE_CODE = 10001;
    public static final String TAG="TAG";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        database = FirebaseDatabase.getInstance();
        myref = database.getReference("datauser");

        biouser = findViewById(R.id.bio);
        user = findViewById(R.id.nameprof);
        emailprofile =findViewById(R.id.emailprof);
        pass = findViewById(R.id.passwordprof);
        confirm =findViewById(R.id.newpass);
        upload = findViewById(R.id.image);

    }


    public void add (View view) {

        String Bio = biouser.getText().toString();
        String nama = user.getText().toString();
        String email = emailprofile.getText().toString();
        String password = pass.getText().toString();
        String confpass = confirm.getText().toString();


        if (!TextUtils.isEmpty(Bio) || !TextUtils.isEmpty(nama) || !TextUtils.isEmpty(email) || !TextUtils.isEmpty(password) || !TextUtils.isEmpty(confpass)) {
            DataUser profile = new DataUser (Bio,nama,email,password,confpass);
            myref.child(nama).setValue(profile);
            Toast.makeText(this,"Congrats! Insert data success",Toast.LENGTH_SHORT).show();
        }
    }

    public void edit(View view) {

        String Bio = biouser.getText().toString();
        String nama = user.getText().toString();
        String email = emailprofile.getText().toString();
        String password = pass.getText().toString();
        String confpass = confirm.getText().toString();


        if (!TextUtils.isEmpty(Bio) || !TextUtils.isEmpty(nama) || !TextUtils.isEmpty(email) || !TextUtils.isEmpty(password)|| !TextUtils.isEmpty(confpass)) {
            DataUser profile = new DataUser(Bio,nama,email,password,confpass);
            myref.child(nama).setValue(profile);
            myref.getDatabase();
            Toast.makeText(this,"changes data success",Toast.LENGTH_SHORT).show();
        }
    }

    public void delete(View view) {

        String Bio = biouser.getText().toString();
        String nama = user.getText().toString();
        String email = emailprofile.getText().toString();
        String password = pass.getText().toString();
        String confpass = confirm.getText().toString();


        if (!TextUtils.isEmpty(Bio) || !TextUtils.isEmpty(nama) || !TextUtils.isEmpty(email) || !TextUtils.isEmpty(password)|| !TextUtils.isEmpty(confpass)) {
            DatabaseReference dR = FirebaseDatabase.getInstance().getReference("datauser").child(nama);
            dR.removeValue();
            Toast.makeText(this,"Delete data success",Toast.LENGTH_SHORT).show();
        }
    }


    public void viewAll(View view) {

        myref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                StringBuffer buffer = new StringBuffer();

                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    DataUser profile = postSnapshot.getValue(DataUser.class);
                    buffer.append("User Bio: " +profile.getBio() + "\n");
                    buffer.append("Email: " + profile.getDatauser() + "\n");
                    buffer.append("Username: " + profile.getNama() + "\n");
                    buffer.append("Password: " +profile.getPassword() + "\n");
                    buffer.append("Confirm Password: " +profile.getConfpass() + "\n");
                    buffer.append("\n");
                }
                showMessage("User Profile", buffer.toString());
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });
    }

    public void showMessage(String title, String Message){
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setCancelable(true);
        dialog.setTitle(title);
        dialog.setMessage(Message);
        dialog.show();
    }

    public void uploadimage(View view) {
        Intent she = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (she.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(she, TAKE_IMAGE_CODE);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == TAKE_IMAGE_CODE) {
            switch (resultCode){
                case RESULT_OK:
                    Bitmap fra = (Bitmap) data.getExtras().get("data");
                    upload.setImageBitmap(fra);
                    handleUpload(fra);

            }
        }
    }

    private void handleUpload(Bitmap fra) {
        ByteArrayOutputStream sher = new ByteArrayOutputStream();
        fra.compress(Bitmap.CompressFormat.JPEG,100, sher);

        String uid = FirebaseAuth.getInstance().getCurrentUser().getUid();
        final StorageReference fratista = FirebaseStorage.getInstance().getReference()
                .child("ProfileImages")
                .child(uid + ".jpeg");
        fratista.putBytes(sher.toByteArray())
                .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        getDownloadUrl(fratista);
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.e(TAG, "onFailure", e.getCause());
                    }
                });
    }

    private void getDownloadUrl(StorageReference fratista) {
        fratista.getDownloadUrl()
                .addOnSuccessListener(new OnSuccessListener<Uri>() {
                    @Override
                    public void onSuccess(Uri uri) {
                        Log.d(TAG, "onSucces" + uri);
                        setUserProfileUrl(uri);
                    }
                });
    }
    private void setUserProfileUrl(Uri uri) {
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        UserProfileChangeRequest request = new UserProfileChangeRequest.Builder()
                .setPhotoUri(uri)
                .build();

        user.updateProfile(request)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Toast.makeText(ProfileActivity.this, "Updates Succesfully",Toast.LENGTH_SHORT).show();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(ProfileActivity.this, "Profile Image Failed..",Toast.LENGTH_SHORT).show();

                    }
                });

    }
}
