package com.example.kosinaja;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.kosinaja.Helper.LocaleHelper;

import io.paperdb.Paper;


public class HomeFragment extends Fragment {
    ImageView imgreview;
    LinearLayout carikost;
    LinearLayout  homeinput, profileUser, forumJualBeli;
    TextView mProfile, mEditProfile, mInputKost, mInputHere, mForum, mForumHere, mSearch, mSearchhere, mReview, mReviewhere;

    protected void attachBaseContext(Context newBase) {
        super.onAttach(LocaleHelper.OnAttach(newBase,"en"));
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v= inflater.inflate(R.layout.fragment_home, container, false);
        setHasOptionsMenu(true);


        homeinput = v.findViewById(R.id.home_kost);
        homeinput.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent home = new Intent(getActivity(), homeInput.class);
                startActivity(home);
            }
        });

        mProfile = (TextView)v.findViewById(R.id.profilee);

        mEditProfile = (TextView)v.findViewById(R.id.edit_prof);

        mInputKost = (TextView)v.findViewById(R.id.inputkost);

        mInputHere = (TextView)v.findViewById(R.id.input_here);

        mForum = (TextView)v.findViewById(R.id.forumm);

        mForumHere = (TextView)v.findViewById(R.id.forum_here);

        mSearch = (TextView)v.findViewById(R.id.searchh);

        mSearchhere = (TextView)v.findViewById(R.id.search_here);

        mReview = (TextView)v.findViewById(R.id.revieww);

        mReviewhere = (TextView)v.findViewById(R.id.review_here);


        Paper.init(getActivity());


        String language = Paper.book().read("language");
        if (language == null)
            Paper.book().write("language", "en");

        updateView((String) Paper.book().read("language"));


        carikost=v.findViewById(R.id.carikost);
        carikost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(),KostList.class);
                startActivity(intent);
            }
        });

        imgreview =v. findViewById(R.id.imgreview);

        imgreview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(),score.class);
                startActivity(i);
            }
        });
        profileUser=v.findViewById(R.id.profileusershe);
        profileUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent profil = new Intent(getActivity(),ProfileActivity.class);
        startActivity(profil);
            }
        });
        forumJualBeli=v.findViewById(R.id.forumJual);
        forumJualBeli.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent profil = new Intent(getActivity(),Activity_Feed.class);
                startActivity(profil);
            }
        });

return v;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_main, menu);
        super.onCreateOptionsMenu(menu,inflater);
        int nightMode = AppCompatDelegate.getDefaultNightMode();
        if(nightMode == AppCompatDelegate.MODE_NIGHT_NO){
            menu.findItem(R.id.night_mode).setTitle(R.string.day_mode);
        } else{
            menu.findItem(R.id.night_mode).setTitle(R.string.night_mode);
        }
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        if (item.getItemId() == R.id.language_indonesia) {
            Paper.book().write("language", "in");
            updateView((String) Paper.book().read("language"));
        } else if (item.getItemId() == R.id.language_english) {
            Paper.book().write("language", "en");
            updateView((String) Paper.book().read("language"));

        }

        if (item.getItemId() == R.id.night_mode) {
            // Get the night mode state of the app.
        int nightMode = AppCompatDelegate.getDefaultNightMode();
            // Set the theme mode for the restarted activity.
            if (nightMode == AppCompatDelegate.MODE_NIGHT_YES) {
                AppCompatDelegate.setDefaultNightMode
                        (AppCompatDelegate.MODE_NIGHT_NO);
            } else {
                AppCompatDelegate.setDefaultNightMode
                        (AppCompatDelegate.MODE_NIGHT_YES);


//
//            if (item.getItemId() == R.id.language_indonesia) {
//                Paper.book().write("language","in");
//                updateView((String) Paper.book().read("language"));
//            } else if (item.getItemId() == R.id.language_english) {
//                Paper.book().write("language", "en");
//                updateView((String) Paper.book().read("language"));
            }
                // Recreate the activity for the theme change to take effect.
            getActivity().recreate();
        }
        return true;
    }

//
//
//
//
////    @Override
////    public void onClick(View v) {
////        switch (v.getId()){
////            case R.id.carikost:
////                Intent intent = new Intent(HomePageActivity.this,KostList.class);
////                startActivity(intent);
////                break;
////        }
//
    private void updateView(String lang) {
        Context context = LocaleHelper.setLocale(getActivity(),lang);
        Resources resources = context.getResources();

        mProfile.setText(resources.getString(R.string.Profile));
        mEditProfile.setText(resources.getString(R.string.Edit_Profile));

        mInputKost.setText(resources.getString(R.string.Input_Kost));
        mInputHere.setText(resources.getString(R.string.Input_here));

        mForum.setText(resources.getString(R.string.forum));
        mForumHere.setText(resources.getString(R.string.Discuss_Here));

        mSearch.setText(resources.getString(R.string.Search));
        mSearchhere.setText(resources.getString(R.string.Search_Here));

        mReview.setText(resources.getString(R.string.Review));
        mReviewhere.setText(resources.getString(R.string.Your_Review));

    }

}
