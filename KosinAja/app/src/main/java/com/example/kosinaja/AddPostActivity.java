package com.example.kosinaja;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Objects;

public class AddPostActivity extends AppCompatActivity implements View.OnClickListener {

    FirebaseStorage storage;
    StorageReference storageReference;
    FirebaseAuth mAuth;
    DatabaseReference db;
    FirebaseUser user;

    EditText etPost;
    Button btnPost,btnUpload;
    ImageView imgPhoto;
    Uri filepath;
    int PICK_IMAGE_REQUEST = 71;
    ProgressDialog loading;
    public Uri donwload;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_post);

        etPost = findViewById(R.id.et_post);
        btnPost = findViewById(R.id.btn_post);
        imgPhoto = findViewById(R.id.img_post);
        btnUpload = findViewById(R.id.btn_uploadphoto);
        mAuth = FirebaseAuth.getInstance();
        db = FirebaseDatabase.getInstance().getReference("fjbPosts");
        loading = new ProgressDialog(this);
        user = mAuth.getCurrentUser();

        btnPost.setOnClickListener(this);
        btnUpload.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_post:
                uploadImage();
                loading.setTitle("Uploading....");
                loading.show();
                break;
            case R.id.btn_uploadphoto:
                Intent galery = new Intent();
                galery.setType("image/*");
                galery.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(galery,"Select Photo"),PICK_IMAGE_REQUEST);
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_IMAGE_REQUEST && RESULT_OK == resultCode&& data != null && data.getData() != null){
            filepath = data.getData();
            try {
                Bitmap photo = MediaStore.Images.Media.getBitmap(getContentResolver(), filepath);
                imgPhoto.setImageBitmap(photo);

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    public  void uploadImage(){

        storage = FirebaseStorage.getInstance();
        storageReference = storage.getReference();
        if (filepath != null) {
            String filename = "images/" + user.getEmail() + db.push().getKey();
            StorageReference ref = storageReference.child(filename);
            ref.putFile(filepath).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    Task<Uri> uriTask = taskSnapshot.getStorage().getDownloadUrl();
                    while (!uriTask.isSuccessful()) ;
                    donwload = uriTask.getResult();
                    String id = db.push().getKey();
                    HashMap<Object, String> data = new HashMap<>();
                    data.put("id", id);
                    data.put("email", user.getEmail());
                    data.put("post", etPost.getText().toString());
                    data.put("image", donwload.toString());
                    db.child(id).setValue(data);
                    Toast.makeText(AddPostActivity.this, "Berhasil", Toast.LENGTH_SHORT).show();
                    loading.dismiss();
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                }
            });
        }else{
            Toast.makeText(this, "error upload foto", Toast.LENGTH_SHORT).show();
        }
    }
}
