package com.example.kosinaja;

public class DataKost {
    private int Image1;
    private String name;
    String title;
    String image;
    String gender;
    String description;
    String harga;
    String stok;
    String alamat;
    String lokasi;
    String waktu;
    String search;
    String latitude;
    String longitude;

    public DataKost(){}


    public DataKost(int image1, String name, String title, String image, String gender, String description, String harga, String stok, String alamat, String lokasi, String waktu, String search, String latitude, String longitude) {
        Image1 = image1;
        this.name = name;
        this.title = title;
        this.image = image;
        this.gender = gender;
        this.description = description;
        this.harga = harga;
        this.stok = stok;
        this.alamat = alamat;
        this.lokasi = lokasi;
        this.waktu = waktu;
        this.search = search;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public int getImage1() {
        return Image1;
    }

    public void setImage1(int image1) {
        Image1 = image1;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getHarga() {
        return harga;
    }

    public void setHarga(String harga) {
        this.harga = harga;
    }

    public String getStok() {
        return stok;
    }

    public void setStok(String stok) {
        this.stok = stok;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getLokasi() {
        return lokasi;
    }

    public void setLokasi(String lokasi) {
        this.lokasi = lokasi;
    }

    public String getWaktu() {
        return waktu;
    }

    public void setWaktu(String waktu) {
        this.waktu = waktu;
    }

    public String getSearch() {
        return search;
    }

    public void setSearch(String search) {
        this.search = search;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }
}
