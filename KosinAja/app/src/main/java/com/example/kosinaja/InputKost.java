package com.example.kosinaja;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

public class InputKost extends AppCompatActivity {

    EditText post_title, post_desc, post_alamat, post_gender, post_harga, post_location, post_search, post_stock, post_time;
    ImageView post_image;
    Button postbtn;



    //folder path for firebase storage
    String mStoragePath = "All_Image_Uploads/";
    //root database name for firebase database
    String mDatabasePath = "Data";
    //creating URI
    Uri mFilePathUri;
    //creating storage reference and database reference
    StorageReference mStorageReference;
    DatabaseReference mDatabaseReference;
    //progress dialog
    ProgressDialog mProgressDialog;
    //image request code for choosing image
    int IMAGE_REQUEST_CODE = 5;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_input_kost);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("KosinAja");

        post_title = findViewById(R.id.new_post_title);
        post_desc = findViewById(R.id.new_post_desc);
        post_alamat = findViewById(R.id.new_post_alamat);
        post_gender = findViewById(R.id.new_post_gender);
        post_harga = findViewById(R.id.new_post_harga);
        post_image = findViewById(R.id.new_post_image);
        post_location = findViewById(R.id.new_post_location);
        post_search = findViewById(R.id.new_post_search);
        post_stock = findViewById(R.id.new_post_stock);
        post_time = findViewById(R.id.new_post_time);
        postbtn = findViewById(R.id.post_btn);



        // image click  to choose image
        post_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //creating intent
                Intent intent = new Intent();
                //setting intent type  as image to select image from phone storage
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "pilih gambar"), IMAGE_REQUEST_CODE);
            }

        });

        //button click  to upload data to firebase
        postbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //call method to upload data to firebase
                uploadDataToFirebase();



            }


        });





        //assign FirebaseStorage instance to storage reference object
        mStorageReference = FirebaseStorage.getInstance().getReference();
        //assign FirebaseDatabase instance with root database name
        mDatabaseReference = FirebaseDatabase.getInstance().getReference(mDatabasePath);
        //progress dialog
        mProgressDialog = new ProgressDialog ( InputKost.this);

    }
    private void uploadDataToFirebase(){
        //check whether filepathuri is empty or not
        if(mFilePathUri !=null){
            //setting progress bar title
            mProgressDialog.setTitle("sedang mengupload gambar...");
            //show progress dialog
            mProgressDialog.show();
            //create second storagereference
            StorageReference storageReference2nd = mStorageReference.child(mStoragePath + System.currentTimeMillis()+ "." + getFileExtension(mFilePathUri));

            //adding addOnSuccessListener to storageReference2nd
            storageReference2nd.putFile(mFilePathUri)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>(){
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot){
                            Task<Uri> urlTask = taskSnapshot.getStorage().getDownloadUrl();
                            while (!urlTask.isSuccessful());
                            Uri downloadUrl = urlTask.getResult();

                            //get title
                            String title = post_title.getText().toString().trim();
                            //get description
                            String description = post_desc.getText().toString().trim();
                            //get alamat
                            String alamat = post_alamat.getText().toString().trim();
                            //get gender
                            String gender = post_gender.getText().toString().trim();
                            //get price
                            String harga = post_harga.getText().toString().trim();
                            String lokasi = post_location.getText().toString().trim();
                            String search = post_search.getText().toString().trim();
                            String stock = post_stock.getText().toString().trim();
                            String waktu = post_time.getText().toString().trim();
                            //hid progress dialog
                            mProgressDialog.dismiss();
                            //show toast that image is uploaded
                            Toast.makeText(InputKost.this,"Data Kost Terkirim",Toast.LENGTH_SHORT).show();
                            DataPost dataInfo =  new DataPost(title, description, alamat, gender, harga, lokasi, search, stock, waktu, downloadUrl.toString());
                            //getting image upload id
                            String data = mDatabaseReference.push().getKey();
                            //adding image upload id's child element into databaseReference
                            mDatabaseReference.child(data).setValue(dataInfo);
                        }
                    })
                    //if something goes wrong such as network failure etc
                    .addOnFailureListener(new OnFailureListener(){
                        @Override
                        public void onFailure(@NonNull Exception e){
                            //hide progress dialog
                            mProgressDialog.dismiss();
                            //show error toast
                            Toast.makeText(InputKost.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    })
                    .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>(){
                        @Override
                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot){
                            mProgressDialog.setTitle(" mengirim...");

                        }
                    });
        }
        else{
            Toast.makeText(this, "Lengkapi data diatas", Toast.LENGTH_SHORT).show();
        }

    }
    //method to get  the selected image file  extension from file path uri
    private String getFileExtension(Uri uri){
        ContentResolver contentResolver = getContentResolver();
        MimeTypeMap mimeTypeMap = MimeTypeMap.getSingleton();
        //returning the file extension
        return mimeTypeMap.getExtensionFromMimeType(contentResolver.getType(uri));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == IMAGE_REQUEST_CODE
                && resultCode == RESULT_OK
                && data != null
                && data.getData() !=null){
            mFilePathUri = data.getData();

            try {
                //getting selected image into bitmap

                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), mFilePathUri);
                //setting bitmap into imageview
                post_image.setImageBitmap(bitmap);
            }
            catch (Exception e){
                Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }
    }

}

