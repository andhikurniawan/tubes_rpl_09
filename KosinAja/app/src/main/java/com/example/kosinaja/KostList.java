package com.example.kosinaja;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.ViewCompat;
import androidx.core.view.ViewConfigurationCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.android.material.tabs.TabLayout;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

import org.jetbrains.annotations.NotNull;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

public class KostList extends AppCompatActivity {

    RecyclerView mRecyclerView;
    FirebaseDatabase mfirebaseDatabase;
    DatabaseReference mRef;
    EditText mySearchView;
    LinearLayoutManager mLayoutManager;
    SharedPreferences mSharePreferences;
    AlertDialog.Builder dialog;
    LayoutInflater inflater;
    View dialogView;
    EditText txtGender;
    EditText txtLokasi;
    EditText txtWaktu;
    TextView txtLokasiSearch;
    private Spinner spinnerWaktu;
    private Spinner spinnerGender;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kost_list);

        mRecyclerView = findViewById(R.id.recyclerView);
        mRecyclerView.setHasFixedSize(true);
        mySearchView = findViewById(R.id.searchView);
        mLayoutManager=new LinearLayoutManager(KostList.this);

        mRecyclerView.setLayoutManager(mLayoutManager);
        mfirebaseDatabase = FirebaseDatabase.getInstance();
        mRef = mfirebaseDatabase.getReference("Data");

        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("Cari Kost");
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowHomeEnabled(true);

        mSharePreferences = getSharedPreferences("SortSetting", MODE_PRIVATE);
        String mSorting = mSharePreferences.getString("Sort", "newest");

        if (mSorting.equals("newest")) {
            mLayoutManager.setReverseLayout(true);
            mLayoutManager.setStackFromEnd(true);
        } else if (mSorting.equals("oldest")) {
            mLayoutManager.setReverseLayout(false);
            mLayoutManager.setStackFromEnd(false);
        }

//        ViewCompat.setNestedScrollingEnabled(mRecyclerView, false);

        txtLokasiSearch=findViewById(R.id.places);
        txtLokasiSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(KostList.this, TabKost.class);
                startActivity(intent);
            }
        });

    }

    private void firebaseSearch(String searchText) {

        String query = searchText.toLowerCase();

        Query firebaseSearchQuery = mRef.orderByChild("search").startAt(query).endAt(query + "\uf8ff");

        FirebaseRecyclerAdapter<DataKost, HolderView> firebaseRecyclerAdapter =
                new FirebaseRecyclerAdapter<DataKost, HolderView>(
                        DataKost.class,
                        R.layout.row,
                        HolderView.class,
                        firebaseSearchQuery
                ) {
                    @Override
                    protected void populateViewHolder(final HolderView holder, DataKost model, int i) {
                        holder.setDetails(getApplicationContext(), model.getTitle(), model.getImage(), model.getDescription(),
                                model.getHarga(), model.getGender(), model.getStok(), model.getAlamat(), model.getLokasi(), model.getWaktu(),model.getLatitude(),model.getLongitude());
                        holder.btn_detail.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                //Get data
                                String rtitle = holder.mTitle.getText().toString();
                                String rdesc = holder.mDesc.getText().toString();
                                String rharga = holder.mHarga.getText().toString();
                                String rstok = holder.mStok.getText().toString();
                                String rgender = holder.mGender.getText().toString();
                                String ralamat = holder.mAlamat.getText().toString();
                                String rlokasi = holder.mLokasi.getText().toString();
                                String rWaktu = holder.mWaktu.getText().toString();
                                String rLatitude=holder.mLatitude.getText().toString();
                                String rLongitude=holder.mLongitude.getText().toString();
                                Drawable drawable = holder.mImageView.getDrawable();
                                Bitmap bitmap = ((BitmapDrawable)drawable).getBitmap();

                                //pass data to detail activity
                                Intent intent = new Intent(KostList.this, DetailKost.class);
                                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                                bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
                                byte[] bytes = stream.toByteArray();
                                intent.putExtra("image", bytes);
                                intent.putExtra("judul",rtitle );
                                intent.putExtra("description", rdesc);
                                intent.putExtra("harga", rharga);
                                intent.putExtra("stok", rstok);
                                intent.putExtra("gender", rgender);
                                intent.putExtra("alamat", ralamat);
                                intent.putExtra("lokasi", rlokasi);
                                intent.putExtra("waktu", rWaktu);
                                intent.putExtra("lat", rLatitude);
                                intent.putExtra("lng", rLongitude);

                                startActivity(intent);
                            }
                        });

                    }
                    @Override
                    public HolderView onCreateViewHolder(ViewGroup parent, int viewType) {
                        HolderView holderView = super.onCreateViewHolder(parent, viewType);

                        holderView.setOnClickListener(new HolderView.ClickListener() {
                            @Override
                            public void onItemClick(View view, int position) {
                                TextView mTitle = view.findViewById(R.id.rTitle);
                                TextView mDesc = view.findViewById(R.id.rDescription);
                                TextView mHarga = view.findViewById(R.id.rHarga);
                                TextView mStok = view.findViewById(R.id.rStok);
                                TextView mGender = view.findViewById(R.id.rGender);
                                TextView mAlamat = view.findViewById(R.id.rAlamat);
                                TextView mLokasi = view.findViewById(R.id.rLokasi);
                                TextView mWaktu = view.findViewById(R.id.rWaktu);
                                TextView mLatitude= view.findViewById(R.id.latitude);
                                TextView mLongitude= view.findViewById(R.id.longitude);
                                ImageView mImageView = view.findViewById(R.id.rImage);

                                //Get data
                                String rtitle = mTitle.getText().toString();
                                String rdesc = mDesc.getText().toString();
                                String rharga = mHarga.getText().toString();
                                String rstok = mStok.getText().toString();
                                String rgender = mGender.getText().toString();
                                String ralamat = mAlamat.getText().toString();
                                String rlokasi = mLokasi.getText().toString();
                                String rWaktu = mWaktu.getText().toString();
                                String rLatitude=mLatitude.getText().toString();
                                String rLongitude=mLongitude.getText().toString();
                                Drawable drawable = mImageView.getDrawable();
                                Bitmap bitmap = ((BitmapDrawable)drawable).getBitmap();

                                //pass data to detail activity
                                Intent intent = new Intent(view.getContext(), DetailKost.class);
                                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                                bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
                                byte[] bytes = stream.toByteArray();
                                intent.putExtra("image", bytes);
                                intent.putExtra("judul",rtitle );
                                intent.putExtra("description", rdesc);
                                intent.putExtra("harga", rharga);
                                intent.putExtra("stok", rstok);
                                intent.putExtra("gender", rgender);
                                intent.putExtra("alamat", ralamat);
                                intent.putExtra("lokasi", rlokasi);
                                intent.putExtra("waktu", rWaktu);
                                intent.putExtra("lat", rLatitude);
                                intent.putExtra("lng", rLongitude);

                                startActivity(intent);
                            }

                            @Override
                            public void onItemLongClick(View view, int position) {

                            }
                        });
                        return holderView;
                    }
                };
        mRecyclerView.setAdapter(firebaseRecyclerAdapter);

    }


    private void firebaseFilterGender(String searchText) {

        Query firebaseSearchQuery = mRef.orderByChild("gender").startAt(searchText).endAt(searchText + "\uf8ff");

        FirebaseRecyclerAdapter<DataKost, HolderView> firebaseRecyclerAdapter =
                new FirebaseRecyclerAdapter<DataKost, HolderView>(
                        DataKost.class,
                        R.layout.row,
                        HolderView.class,
                        firebaseSearchQuery
                ) {
                    @Override
                    protected void populateViewHolder(final HolderView holder, DataKost model, int i) {
                        holder.setDetails(getApplicationContext(), model.getTitle(), model.getImage(), model.getDescription(),
                                model.getHarga(), model.getGender(), model.getStok(), model.getAlamat(), model.getLokasi(), model.getWaktu(),model.getLatitude(),model.getLongitude());
                        holder.btn_detail.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                //Get data
                                String rtitle = holder.mTitle.getText().toString();
                                String rdesc = holder.mDesc.getText().toString();
                                String rharga = holder.mHarga.getText().toString();
                                String rstok = holder.mStok.getText().toString();
                                String rgender = holder.mGender.getText().toString();
                                String ralamat = holder.mAlamat.getText().toString();
                                String rlokasi = holder.mLokasi.getText().toString();
                                String rWaktu = holder.mWaktu.getText().toString();
                                String rLatitude=holder.mLatitude.getText().toString();
                                String rLongitude=holder.mLongitude.getText().toString();
                                Drawable drawable = holder.mImageView.getDrawable();
                                Bitmap bitmap = ((BitmapDrawable)drawable).getBitmap();

                                //pass data to detail activity
                                Intent intent = new Intent(KostList.this, DetailKost.class);
                                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                                bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
                                byte[] bytes = stream.toByteArray();
                                intent.putExtra("image", bytes);
                                intent.putExtra("judul",rtitle );
                                intent.putExtra("description", rdesc);
                                intent.putExtra("harga", rharga);
                                intent.putExtra("stok", rstok);
                                intent.putExtra("gender", rgender);
                                intent.putExtra("alamat", ralamat);
                                intent.putExtra("lokasi", rlokasi);
                                intent.putExtra("waktu", rWaktu);
                                intent.putExtra("lat", rLatitude);
                                intent.putExtra("lng", rLongitude);

                                startActivity(intent);
                            }
                        });

                    }
                    @Override
                    public HolderView onCreateViewHolder(ViewGroup parent, int viewType) {
                        HolderView holderView = super.onCreateViewHolder(parent, viewType);

                        holderView.setOnClickListener(new HolderView.ClickListener() {
                            @Override
                            public void onItemClick(View view, int position) {
                                TextView mTitle = view.findViewById(R.id.rTitle);
                                TextView mDesc = view.findViewById(R.id.rDescription);
                                TextView mHarga = view.findViewById(R.id.rHarga);
                                TextView mStok = view.findViewById(R.id.rStok);
                                TextView mGender = view.findViewById(R.id.rGender);
                                TextView mAlamat = view.findViewById(R.id.rAlamat);
                                TextView mLokasi = view.findViewById(R.id.rLokasi);
                                TextView mWaktu = view.findViewById(R.id.rWaktu);
                                TextView mLatitude= view.findViewById(R.id.latitude);
                                TextView mLongitude= view.findViewById(R.id.longitude);
                                ImageView mImageView = view.findViewById(R.id.rImage);

                                //Get data
                                String rtitle = mTitle.getText().toString();
                                String rdesc = mDesc.getText().toString();
                                String rharga = mHarga.getText().toString();
                                String rstok = mStok.getText().toString();
                                String rgender = mGender.getText().toString();
                                String ralamat = mAlamat.getText().toString();
                                String rlokasi = mLokasi.getText().toString();
                                String rWaktu = mWaktu.getText().toString();
                                String rLatitude=mLatitude.getText().toString();
                                String rLongitude=mLongitude.getText().toString();
                                Drawable drawable = mImageView.getDrawable();
                                Bitmap bitmap = ((BitmapDrawable)drawable).getBitmap();

                                //pass data to detail activity
                                Intent intent = new Intent(view.getContext(), DetailKost.class);
                                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                                bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
                                byte[] bytes = stream.toByteArray();
                                intent.putExtra("image", bytes);
                                intent.putExtra("judul",rtitle );
                                intent.putExtra("description", rdesc);
                                intent.putExtra("harga", rharga);
                                intent.putExtra("stok", rstok);
                                intent.putExtra("gender", rgender);
                                intent.putExtra("alamat", ralamat);
                                intent.putExtra("lokasi", rlokasi);
                                intent.putExtra("waktu", rWaktu);
                                intent.putExtra("lat", rLatitude);
                                intent.putExtra("lng", rLongitude);

                                startActivity(intent);
                            }

                            @Override
                            public void onItemLongClick(View view, int position) {

                            }
                        });
                        return holderView;
                    }
                };
        mRecyclerView.setAdapter(firebaseRecyclerAdapter);

    }
    private void firebaseFilterJangkaWaktu(String searchText) {

        Query firebaseSearchQuery = mRef.orderByChild("waktu").startAt(searchText).endAt(searchText + "\uf8ff");

        FirebaseRecyclerAdapter<DataKost, HolderView> firebaseRecyclerAdapter =
                new FirebaseRecyclerAdapter<DataKost, HolderView>(
                        DataKost.class,
                        R.layout.row,
                        HolderView.class,
                        firebaseSearchQuery
                ) {
                    @Override
                    protected void populateViewHolder(final HolderView holder, DataKost model, int i) {
                        holder.setDetails(getApplicationContext(), model.getTitle(), model.getImage(), model.getDescription(),
                                model.getHarga(), model.getGender(), model.getStok(), model.getAlamat(), model.getLokasi(),model.getWaktu(),model.getLatitude(),model.getLongitude());
                        holder.btn_detail.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                //Get data
                                String rtitle = holder.mTitle.getText().toString();
                                String rdesc = holder.mDesc.getText().toString();
                                String rharga = holder.mHarga.getText().toString();
                                String rstok = holder.mStok.getText().toString();
                                String rgender = holder.mGender.getText().toString();
                                String ralamat = holder.mAlamat.getText().toString();
                                String rlokasi = holder.mLokasi.getText().toString();
                                String rWaktu = holder.mWaktu.getText().toString();
                                String rLatitude=holder.mLatitude.getText().toString();
                                String rLongitude=holder.mLongitude.getText().toString();
                                Drawable drawable = holder.mImageView.getDrawable();
                                Bitmap bitmap = ((BitmapDrawable)drawable).getBitmap();

                                //pass data to detail activity
                                Intent intent = new Intent(KostList.this, DetailKost.class);
                                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                                bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
                                byte[] bytes = stream.toByteArray();
                                intent.putExtra("image", bytes);
                                intent.putExtra("judul",rtitle );
                                intent.putExtra("description", rdesc);
                                intent.putExtra("harga", rharga);
                                intent.putExtra("stok", rstok);
                                intent.putExtra("gender", rgender);
                                intent.putExtra("alamat", ralamat);
                                intent.putExtra("lokasi", rlokasi);
                                intent.putExtra("waktu", rWaktu);
                                intent.putExtra("lat", rLatitude);
                                intent.putExtra("lng", rLongitude);

                                startActivity(intent);
                            }
                        });

                    }

                    @Override
                    public HolderView onCreateViewHolder(ViewGroup parent, int viewType) {
                        HolderView holderView = super.onCreateViewHolder(parent, viewType);

                        holderView.setOnClickListener(new HolderView.ClickListener() {
                            @Override
                            public void onItemClick(View view, int position) {
                                TextView mTitle = view.findViewById(R.id.rTitle);
                                TextView mDesc = view.findViewById(R.id.rDescription);
                                TextView mHarga = view.findViewById(R.id.rHarga);
                                TextView mStok = view.findViewById(R.id.rStok);
                                TextView mGender = view.findViewById(R.id.rGender);
                                TextView mAlamat = view.findViewById(R.id.rAlamat);
                                TextView mLokasi = view.findViewById(R.id.rLokasi);
                                TextView mWaktu = view.findViewById(R.id.rWaktu);
                                TextView mLatitude= view.findViewById(R.id.latitude);
                                TextView mLongitude= view.findViewById(R.id.longitude);
                                ImageView mImageView = view.findViewById(R.id.rImage);

                                //Get data
                                String rtitle = mTitle.getText().toString();
                                String rdesc = mDesc.getText().toString();
                                String rharga = mHarga.getText().toString();
                                String rstok = mStok.getText().toString();
                                String rgender = mGender.getText().toString();
                                String ralamat = mAlamat.getText().toString();
                                String rlokasi = mLokasi.getText().toString();
                                String rWaktu = mWaktu.getText().toString();
                                String rLatitude=mLatitude.getText().toString();
                                String rLongitude=mLongitude.getText().toString();
                                Drawable drawable = mImageView.getDrawable();
                                Bitmap bitmap = ((BitmapDrawable)drawable).getBitmap();

                                //pass data to detail activity
                                Intent intent = new Intent(view.getContext(), DetailKost.class);
                                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                                bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
                                byte[] bytes = stream.toByteArray();
                                intent.putExtra("image", bytes);
                                intent.putExtra("judul",rtitle );
                                intent.putExtra("description", rdesc);
                                intent.putExtra("harga", rharga);
                                intent.putExtra("stok", rstok);
                                intent.putExtra("gender", rgender);
                                intent.putExtra("alamat", ralamat);
                                intent.putExtra("lokasi", rlokasi);
                                intent.putExtra("waktu", rWaktu);
                                intent.putExtra("lat", rLatitude);
                                intent.putExtra("lng", rLongitude);

                                startActivity(intent);
                            }

                            @Override
                            public void onItemLongClick(View view, int position) {

                            }
                        });
                        return holderView;
                    }
                };
        mRecyclerView.setAdapter(firebaseRecyclerAdapter);

    }


    @Override
    protected void onStart() {
        super.onStart();
        FirebaseRecyclerAdapter<DataKost, HolderView> firebaseRecyclerAdapter =
                new FirebaseRecyclerAdapter<DataKost, HolderView>(
                        DataKost.class,
                        R.layout.row,
                        HolderView.class,
                        mRef
                ) {
                    @Override
                    protected void populateViewHolder(final HolderView holder, DataKost model, int i) {
                        holder.setDetails(getApplicationContext(), model.getTitle(), model.getImage(), model.getDescription(),
                                model.getHarga(), model.getGender(), model.getStok(), model.getAlamat(), model.getLokasi(),model.getWaktu(), model.getLatitude(),model.getLongitude());

                        holder.btn_detail.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                //Get data
                                String rtitle = holder.mTitle.getText().toString();
                                String rdesc = holder.mDesc.getText().toString();
                                String rharga = holder.mHarga.getText().toString();
                                String rstok = holder.mStok.getText().toString();
                                String rgender = holder.mGender.getText().toString();
                                String ralamat = holder.mAlamat.getText().toString();
                                String rlokasi = holder.mLokasi.getText().toString();
                                String rWaktu = holder.mWaktu.getText().toString();
                                String rLatitude=holder.mLatitude.getText().toString();
                                String rLongitude=holder.mLongitude.getText().toString();
                                Drawable drawable = holder.mImageView.getDrawable();
                                Bitmap bitmap = ((BitmapDrawable)drawable).getBitmap();

                                //pass data to detail activity
                                Intent intent = new Intent(KostList.this, DetailKost.class);
                                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                                bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
                                byte[] bytes = stream.toByteArray();
                                intent.putExtra("image", bytes);
                                intent.putExtra("judul",rtitle );
                                intent.putExtra("description", rdesc);
                                intent.putExtra("harga", rharga);
                                intent.putExtra("stok", rstok);
                                intent.putExtra("gender", rgender);
                                intent.putExtra("alamat", ralamat);
                                intent.putExtra("lokasi", rlokasi);
                                intent.putExtra("waktu", rWaktu);
                                intent.putExtra("lat", rLatitude);
                                intent.putExtra("lng", rLongitude);

                                startActivity(intent);
                            }
                        });
                    }

                    @NotNull
                    @Override
                    public HolderView onCreateViewHolder(ViewGroup parent, int viewType) {
                        HolderView holderView = super.onCreateViewHolder(parent, viewType);

                        holderView.setOnClickListener(new HolderView.ClickListener() {
                            @Override
                            public void onItemClick(View view, int position) {
                                TextView mTitle = view.findViewById(R.id.rTitle);
                                TextView mDesc = view.findViewById(R.id.rDescription);
                                TextView mHarga = view.findViewById(R.id.rHarga);
                                TextView mStok = view.findViewById(R.id.rStok);
                                TextView mGender = view.findViewById(R.id.rGender);
                                TextView mAlamat = view.findViewById(R.id.rAlamat);
                                TextView mLokasi = view.findViewById(R.id.rLokasi);
                                TextView mWaktu = view.findViewById(R.id.rWaktu);
                                TextView mLatitude= view.findViewById(R.id.latitude);
                                TextView mLongitude= view.findViewById(R.id.longitude);
                                ImageView mImageView = view.findViewById(R.id.rImage);

                                //Get data
                                String rtitle = mTitle.getText().toString();
                                String rdesc = mDesc.getText().toString();
                                String rharga = mHarga.getText().toString();
                                String rstok = mStok.getText().toString();
                                String rgender = mGender.getText().toString();
                                String ralamat = mAlamat.getText().toString();
                                String rlokasi = mLokasi.getText().toString();
                                String rWaktu = mWaktu.getText().toString();
                                String rLatitude=mLatitude.getText().toString();
                                String rLongitude=mLongitude.getText().toString();
                                Drawable drawable = mImageView.getDrawable();
                                Bitmap bitmap = ((BitmapDrawable)drawable).getBitmap();

                                //pass data to detail activity
                                Intent intent = new Intent(view.getContext(), DetailKost.class);
                                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                                bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
                                byte[] bytes = stream.toByteArray();
                                intent.putExtra("image", bytes);
                                intent.putExtra("judul",rtitle );
                                intent.putExtra("description", rdesc);
                                intent.putExtra("harga", rharga);
                                intent.putExtra("stok", rstok);
                                intent.putExtra("gender", rgender);
                                intent.putExtra("alamat", ralamat);
                                intent.putExtra("lokasi", rlokasi);
                                intent.putExtra("waktu", rWaktu);
                                intent.putExtra("lat", rLatitude);
                                intent.putExtra("lng", rLongitude);

                                startActivity(intent);
                            }

                            @Override
                            public void onItemLongClick(View view, int position) {

                            }
                        });
                        return holderView;
                    }
                };

        mRecyclerView.setAdapter(firebaseRecyclerAdapter);
    }

    public void Sorting(View view) {
        showSortDialog();
    }


    private void DialogFilterGender() {
        dialog = new AlertDialog.Builder(KostList.this);
        inflater = getLayoutInflater();
        dialogView = inflater.inflate(R.layout.filter_gender, null);
        dialog.setView(dialogView);
        dialog.setCancelable(true);
        dialog.setIcon(R.drawable.ic_filter);
        dialog.setTitle("Filter Type Kost");

        spinnerGender= (Spinner)dialogView.findViewById(R.id.spGender);

        List<String> cGender = new ArrayList<>();
        cGender.add(0,"Pilih gender");
        cGender.add("Putri");
        cGender.add("Putra");
        cGender.add("Campur");

        ArrayAdapter<String> wAdapter;
        wAdapter= new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, cGender);
        wAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerGender.setAdapter(wAdapter);




        dialog.setPositiveButton("OKE", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String searchText = spinnerGender.getSelectedItem().toString();
//                String searchText = txtLokasi.getText().toString();
                firebaseFilterGender(searchText);


            }
        });
        dialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }
    private void DialogFilterLokasi() {
        dialog = new AlertDialog.Builder(KostList.this);
        inflater = getLayoutInflater();
        dialogView = inflater.inflate(R.layout.filter_lokasi, null);
        dialog.setView(dialogView);
        dialog.setCancelable(true);
        dialog.setIcon(R.drawable.ic_filter);
        dialog.setTitle("Filter by location");

        txtLokasi = (EditText) dialogView.findViewById(R.id.txt_lokasi);


        dialog.setPositiveButton("OKE", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                String searchText = txtLokasi.getText().toString();
                firebaseSearch(searchText);


            }
        });
        dialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }
    private void DialogFilterWaktu() {
        dialog = new AlertDialog.Builder(KostList.this);
        inflater = getLayoutInflater();
        dialogView = inflater.inflate(R.layout.filter_waktu, null);
        dialog.setView(dialogView);
        dialog.setCancelable(true);
        dialog.setIcon(R.drawable.ic_filter);
        dialog.setTitle("Filter by period");

//        txtWaktu = (EditText) dialogView.findViewById(R.id.txt_waktu);
        spinnerWaktu= (Spinner)dialogView.findViewById(R.id.spWaktu);

        List<String> cWaktu = new ArrayList<>();
        cWaktu.add(0,"Pilih period");
        cWaktu.add("bulan");
        cWaktu.add("tahun");

        ArrayAdapter<String> wAdapter;
        wAdapter= new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, cWaktu);
        wAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerWaktu.setAdapter(wAdapter);
//        final String waktu = spinnerWaktu.getSelectedItem().toString().trim();


        dialog.setPositiveButton("OKE", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                String searchText = spinnerWaktu.getSelectedItem().toString();
                firebaseFilterJangkaWaktu(searchText);


            }
        });
        dialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }



    private void showSortDialog() {
        String[] sortOptions = {"Newest", "Oldest"};

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Sort by")
                .setIcon(R.drawable.ic_sort2)
                .setItems(sortOptions, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (which == 0) {

                            SharedPreferences.Editor editor = mSharePreferences.edit();
                            editor.putString("Sort", "newest");
                            editor.apply();
                            recreate();


                        } else if (which == 1) {
                            {
                                SharedPreferences.Editor editor = mSharePreferences.edit();
                                editor.putString("Sort", "oldest");
                                editor.apply();
                                recreate();


                            }
                        }



                    }
                });
        builder.show();
    }

    public void Filter(View view) {
        showFilterDialog();
    }

    public void Search(View view) {
        String searchText = mySearchView.getText().toString();
        firebaseSearch(searchText);
    }



    private void showFilterDialog() {
            String[] sortOptions = {"Gender", "Jangka Waktu", "Lokasi", "Sort by"};

            final AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Filter Category")
                    .setIcon(R.drawable.ic_sort2)
                    .setItems(sortOptions, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            if (which == 0) {

                                DialogFilterGender();
                            } else if (which == 1) {
                                DialogFilterWaktu();


                                }else if(which==2){

                                DialogFilterLokasi();


                                }else if(which==3){
                                    showSortDialog();



                            }

                        }
                    });
            builder.show();
        }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }


    public void goTo(View view) {
        Intent intent = new Intent(KostList.this, TabKost.class);
        startActivity(intent);
    }
}

