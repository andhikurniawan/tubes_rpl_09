package com.example.kosinaja;

public class Data {
    String title;
    String description;
    String alamat;
    String gender;
    String harga;
    String lokasi;
    String search;
    String stok;
    String waktu;

    public Data(String title, String description, String alamat, String gender, String harga, String lokasi, String search, String stok, String waktu, String s) {
        this.title = title;
        this.description = description;
        this.alamat = alamat;
        this.gender = gender;
        this.harga = harga;
        this.lokasi = lokasi;
        this.search = search;
        this.stok = stok;
        this.waktu= waktu;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getAlamat() {
        return alamat;
    }

    public String getGender() {
        return gender;
    }

    public String getHarga() {
        return harga;
    }


    public String getLokasi() {
        return lokasi;
    }

    public String getSearch() {
        return search;
    }

    public String getStok() {
        return stok;
    }

    public String getWaktu() {
        return waktu;
    }

}
