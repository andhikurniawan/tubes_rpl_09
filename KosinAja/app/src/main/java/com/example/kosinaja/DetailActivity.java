package com.example.kosinaja;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.squareup.picasso.Picasso;

public class DetailActivity extends FragmentActivity implements OnMapReadyCallback {
    ImageView imageView;
    TextView textView, mdeskripsi, mharga;
    GoogleMap mMap;
    LatLng latLng;
    Button pesan;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);


        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        imageView = findViewById(R.id.Image);
        textView = findViewById(R.id.Title);
        mdeskripsi = findViewById(R.id.Alamat);
        mharga = findViewById(R.id.Harga);
        pesan= (Button)findViewById(R.id.Btn_Detail);
        pesan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DetailActivity.this, Pesan.class);
                startActivity(intent);
            }
        });

//        final int images = getIntent().getIntExtra("Images", 0);
        final String name  =  getIntent().getStringExtra("title");
        final String deskripsi  =  getIntent().getStringExtra("des");
        final String harga  =  getIntent().getStringExtra("mharga");
        final String image  =  getIntent().getStringExtra("image");



        Picasso.get()
                .load(image)
                .into(imageView);
        textView.setText(name);
        mharga.setText(harga);
        mdeskripsi.setText(deskripsi);

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        final String latitude= getIntent().getStringExtra("lati");
        final String longitude= getIntent().getStringExtra("lngo");
        double lati=Double.parseDouble(latitude);
        double lngo=Double.parseDouble(longitude);
        mMap= googleMap;
        mMap.getUiSettings().setZoomControlsEnabled(true);
        mMap.getUiSettings().setMapToolbarEnabled(true);
        CameraPosition INIT = new CameraPosition.Builder().target(new LatLng(lati,lngo)).zoom(15.5F).bearing(300F)
                .tilt(50F) // viewing angle
                .build();

// use map to move camera into position
        mMap.moveCamera(CameraUpdateFactory.newCameraPosition(INIT));
        mMap.addMarker(new MarkerOptions().position(new LatLng(lati,lngo)).title(getIntent().getStringExtra("title")).snippet(getIntent().getStringExtra("des")).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)));
//        mMap.moveCamera(CameraUpdateFactory.newLatLng(Data6));
//        mMap.animateCamera(CameraUpdateFactory.zoomTo(12));
    }
}
