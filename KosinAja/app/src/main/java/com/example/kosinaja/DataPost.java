package com.example.kosinaja;

public class DataPost {
    String title;
    String description;
    String alamat;
    String gender;
    String harga;
    String lokasi;
    String search;
    String stok;
    String waktu;
    String gambar;

    public DataPost(String title, String description, String alamat, String gender, String harga, String lokasi, String search, String stok, String waktu, String gambar) {
        this.title = title;
        this.description = description;
        this.alamat = alamat;
        this.gender = gender;
        this.harga = harga;
        this.lokasi = lokasi;
        this.search = search;
        this.stok = stok;
        this.gambar=gambar;
        this.waktu = waktu;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getHarga() {
        return harga;
    }

    public void setHarga(String harga) {
        this.harga = harga;
    }

    public String getLokasi() {
        return lokasi;
    }

    public void setLokasi(String lokasi) {
        this.lokasi = lokasi;
    }

    public String getSearch() {
        return search;
    }

    public void setSearch(String search) {
        this.search = search;
    }

    public String getStok() {
        return stok;
    }

    public void setStok(String stok) {
        this.stok = stok;
    }

    public String getWaktu() {
        return waktu;
    }

    public void setWaktu(String waktu) {
        this.waktu = waktu;
    }
}
