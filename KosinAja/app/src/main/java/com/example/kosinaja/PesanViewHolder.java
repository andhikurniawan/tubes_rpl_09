package com.example.kosinaja;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class PesanViewHolder extends RecyclerView.ViewHolder {
    public TextView orderId, ptitle, pharga, txt_bayar, txt_cancel, txt_status;
    private View mView;

    public PesanViewHolder(@NonNull View itemView) {
        super(itemView);
        mView=itemView;
        txt_bayar=mView.findViewById(R.id.bayar_action);
       txt_cancel= mView.findViewById(R.id.batal_action);
       txt_status= mView.findViewById(R.id.status_kost);
        orderId= mView.findViewById(R.id.orderId);
        pharga=mView.findViewById(R.id.tharga);
        ptitle=mView.findViewById(R.id.title_kost);


    }
    public void setPesan(Context ctx, String orderid, String title, String harga){
        orderId= mView.findViewById(R.id.orderId);
        pharga=mView.findViewById(R.id.tharga);
        ptitle=mView.findViewById(R.id.title_kost);

        orderId.setText("Booking ID "+ orderid);
        pharga.setText("Rp. " +harga);
        ptitle.setText(title);
    }
}
