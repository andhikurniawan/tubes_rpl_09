package com.example.kosinaja;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;

import static android.app.Activity.RESULT_OK;


public class ProfileFragment extends Fragment {
    EditText user,emailprofile,pass,confirm,biouser;
    FirebaseDatabase database;
    FirebaseStorage storage;
    StorageReference storageReference;
    DatabaseReference myref;
    ImageView upload;
    int PICK_IMAGE_REQUEST = 71;
    String PROFILE_IMAGE_URL = null;
    int TAKE_IMAGE_CODE = 10001;
    public static final String TAG="TAG";
    Button viewShow, delete;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v= inflater.inflate(R.layout.fragment_profile, container, false);


        database = FirebaseDatabase.getInstance();
        myref = database.getReference("datauser");

        biouser = v.findViewById(R.id.bio);
        user = v.findViewById(R.id.nameprof);
        emailprofile =v.findViewById(R.id.emailprof);
        pass = v.findViewById(R.id.passwordprof);
        confirm =v.findViewById(R.id.newpass);
        upload = v.findViewById(R.id.image);

        delete=v.findViewById(R.id.btnview);
        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myref.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        StringBuffer buffer = new StringBuffer();

                        for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                            DataUser profile = postSnapshot.getValue(DataUser.class);
                            buffer.append("Your Bio: " + profile.getNama() + "\n");
                            buffer.append("Email: " +profile.getPassword() + "\n");
                            buffer.append("Password: " + profile.getDatauser() + "\n");
                            buffer.append("Confirm Password: " +profile.getConfpass() + "\n");
                            buffer.append("\n");
                        }
                        showMessage("User Profile", buffer.toString());
                    }
                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                    }
                });
            }
        });

        viewShow=v.findViewById(R.id.btndelete);
        viewShow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String Bio = biouser.getText().toString();
                String email = emailprofile.getText().toString();
                String password = pass.getText().toString();
                String confpass = confirm.getText().toString();
                String nama = user.getText().toString();


                if (!TextUtils.isEmpty(Bio) || !TextUtils.isEmpty(email) || !TextUtils.isEmpty(password) || !TextUtils.isEmpty(confpass)|| !TextUtils.isEmpty(nama)) {
                    DatabaseReference dR = FirebaseDatabase.getInstance().getReference("datauser").child(nama);
                    dR.removeValue();
                    Toast.makeText(getActivity(),"Delete data success",Toast.LENGTH_SHORT).show();
                }
            }
        });

        return v;
    }

    public void add (View view) {

        final String Bio = biouser.getText().toString();
        final String email = emailprofile.getText().toString();
        final String password = pass.getText().toString();
        final String confpass = confirm.getText().toString();
        final String nama = user.getText().toString();


        if (!TextUtils.isEmpty(Bio) || !TextUtils.isEmpty(email) || !TextUtils.isEmpty(password) || !TextUtils.isEmpty(confpass) || !TextUtils.isEmpty(nama)) {
            DataUser profile = new DataUser(nama, Bio, email, password, confpass);
            myref.child(nama).setValue(profile);
            Toast.makeText(getActivity(), "Congrats! Insert data success", Toast.LENGTH_SHORT).show();

        }
    }

    public void edit(View view) {

        String Bio = biouser.getText().toString();
        String email = emailprofile.getText().toString();
        String password = pass.getText().toString();
        String confpass = confirm.getText().toString();
        String nama = user.getText().toString();


        if (!TextUtils.isEmpty(Bio) || !TextUtils.isEmpty(email) || !TextUtils.isEmpty(password) || !TextUtils.isEmpty(confpass)|| !TextUtils.isEmpty(nama)) {
            DataUser profile = new DataUser(nama,Bio,email,password,confpass);
            myref.child(nama).setValue(profile);
            myref.getDatabase();
            Toast.makeText(getActivity(),"changes data success",Toast.LENGTH_SHORT).show();
        }
    }


    public void showMessage(String title, String Message){
        AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
        dialog.setCancelable(true);
        dialog.setTitle(title);
        dialog.setMessage(Message);
        dialog.show();
    }

    public void uploadimage(View view) {
        Intent she = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (she.resolveActivity(getActivity().getPackageManager()) != null) {
            startActivityForResult(she, TAKE_IMAGE_CODE);
        }
//
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == TAKE_IMAGE_CODE) {
            switch (resultCode){
                case RESULT_OK:
                    Bitmap fra = (Bitmap) data.getExtras().get("data");
                    upload.setImageBitmap(fra);
                    handleUpload(fra);

            }
        }
    }

    private void handleUpload(Bitmap fra) {
        ByteArrayOutputStream sher = new ByteArrayOutputStream();
        fra.compress(Bitmap.CompressFormat.JPEG,100, sher);

        String uid = FirebaseAuth.getInstance().getCurrentUser().getUid();
        final StorageReference fratista = FirebaseStorage.getInstance().getReference()
                .child("ProfileImages")
                .child(uid + ".jpeg");
        fratista.putBytes(sher.toByteArray())
                .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        getDownloadUrl(fratista);
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.e(TAG, "onFailure", e.getCause());
                    }
                });
    }

    private void getDownloadUrl(StorageReference fratista) {
        fratista.getDownloadUrl()
                .addOnSuccessListener(new OnSuccessListener<Uri>() {
                    @Override
                    public void onSuccess(Uri uri) {
                        Log.d(TAG, "onSucces" + uri);
                        setUserProfileUrl(uri);
                    }
                });
    }
    private void setUserProfileUrl(Uri uri) {
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        UserProfileChangeRequest request = new UserProfileChangeRequest.Builder()
                .setPhotoUri(uri)
                .build();

        user.updateProfile(request)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Toast.makeText(getActivity(), "Updates Succesfully",Toast.LENGTH_SHORT).show();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(getActivity(), "Profile Image Failed..",Toast.LENGTH_SHORT).show();

                    }
                });

    }
}