package com.example.kosinaja;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;

import java.util.ArrayList;

public class AdapterFeed extends RecyclerView.Adapter<AdapterFeed.MyViewHolder> {

    static Context mContext;
    static ArrayList<ModelFeed> modelFeedArrayList = new ArrayList<>();
    RequestManager glide;

    public AdapterFeed(Context context, ArrayList<ModelFeed> modelFeedArrayList) {
        this.mContext = context;
        this.modelFeedArrayList = modelFeedArrayList;
        glide = Glide.with(context);

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_feed, parent, false);
        MyViewHolder viewHolder = new MyViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        final ModelFeed modelFeed = modelFeedArrayList.get(position);

        holder.tv_name.setText(modelFeed.getEmail());
        holder.tv_status.setText(modelFeed.getPost());
        glide.load(modelFeed.getImage()).into(holder.imgView_postPic);

    }

    @Override
    public int getItemCount() {
        return modelFeedArrayList.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView tv_name, tv_comments, tv_status;
        ImageView imgView_proPic, imgView_postPic;
        public MyViewHolder(View itemView) {
            super(itemView);

            imgView_postPic = (ImageView) itemView.findViewById(R.id.imgView_postPic);
            tv_name = (TextView) itemView.findViewById(R.id.tv_name);
            tv_status = (TextView) itemView.findViewById(R.id.tv_status);

            tv_status.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.tv_status:
                    ModelFeed current = modelFeedArrayList.get(getAdapterPosition());
                    Intent detail = new Intent(mContext,PostDetailActivity.class);
                    detail.putExtra("id",current.getId());
                    Toast.makeText(mContext, current.getId(), Toast.LENGTH_SHORT).show();
                    mContext.startActivity(detail);
                    break;
            }
        }
    }
}
