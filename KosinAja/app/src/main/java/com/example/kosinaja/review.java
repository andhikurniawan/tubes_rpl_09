package com.example.kosinaja;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.Toast;



import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.HashMap;


public class review extends AppCompatActivity{

    FirebaseStorage storage;
    StorageReference storageReference;
    FirebaseAuth mAuth;
    DatabaseReference db;
    FirebaseUser user;

    Button submit;
    EditText isikomentar;
    ProgressDialog loading;
    public String isikomentar_string;
    public float ratingBar_string;
    public float ratingBar_string2;
    public float ratingBar_string3;
    public String scoreText_string;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_review);

        //declare Firebase objects
        mAuth = FirebaseAuth.getInstance();
        db = FirebaseDatabase.getInstance().getReference("reviewkost");
        user = mAuth.getCurrentUser();

        Intent i = getIntent();
        scoreText_string = i.getStringExtra("scoreText_string");

        loading = new ProgressDialog(this);

/*
        final RatingBar ratingBar = (RatingBar) findViewById(R.id.ratingBar);
        ratingBar.setNumStars(5);

        final RatingBar ratingBar2 = (RatingBar) findViewById(R.id.ratingBar2);
        ratingBar2.setNumStars(5);

        final RatingBar ratingBar3 = (RatingBar) findViewById(R.id.ratingBar3);
        ratingBar3.setNumStars(5);

 */

        isikomentar = findViewById(R.id.isikomentar);
        submit = findViewById(R.id.submit);

        submit.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                isikomentar_string = isikomentar.getText().toString();


                //float ratingBar_string = ratingBar.getRating();
                // db.push().setValue(String.valueOf("Kebersihan : "+ratingBar_string));

                //float ratingBar_string2 = ratingBar2.getRating();
                // db.push().setValue(String.valueOf("Keamanan : " +ratingBar_string2));

                //float ratingBar_string3 = ratingBar3.getRating();
                //db.push().setValue(String.valueOf("Kebersihan : "+ratingBar_string+ " Keamanan : " +ratingBar_string2+" fasilitas : " +ratingBar_string3+""));



                if (TextUtils.isEmpty(isikomentar.getText())) {
                    isikomentar.setError("Silahkan isi komentar");
                    isikomentar.requestFocus();
                }
                else {
                    loading = ProgressDialog.show(review.this, null,"please wait", true, false);
                    submitkomen();
                }
            }
        });
    }

    protected void submitkomen() {
        //inisiasi string id untuk push
        String komentar = db.push().getKey();
        //String keamanan = db.push().getKey();

        final RatingBar ratingBar = (RatingBar) findViewById(R.id.ratingBar);
        ratingBar.setNumStars(5);

        final RatingBar ratingBar2 = (RatingBar) findViewById(R.id.ratingBar2);
        ratingBar2.setNumStars(5);

        final RatingBar ratingBar3 = (RatingBar) findViewById(R.id.ratingBar3);
        ratingBar3.setNumStars(5);

        float ratingBar_string = ratingBar.getRating();

        float ratingBar_string2 = ratingBar2.getRating();

        float ratingBar_string3 = ratingBar3.getRating();
/*
        Intent i = getIntent();
        String scorekosan = i.getStringExtra("SCOREKOSAN");

 */


        HashMap<Object, String> data = new HashMap<>();
        //data.put("id", komentar);
        data.put("Review", isikomentar_string);
        data.put("Fasilitas" , String.valueOf(ratingBar_string3));
        data.put("Keamanan", String.valueOf(ratingBar_string2));
        data.put("Kebersihan", String.valueOf(ratingBar_string));
        data.put("Score", scoreText_string);

        db.child(komentar).setValue(data).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                loading.dismiss();

                Toast.makeText(review.this,"Feedback diterima",Toast.LENGTH_SHORT).show();
                Intent i = new Intent(review.this, listreview.class);
                i.putExtra("isikomentar", isikomentar_string);

                /*i.putExtra("fasilitas", String.valueOf(ratingBar_string));
                i.putExtra("keamanan", String.valueOf(ratingBar_string2));
                i.putExtra("kebersihan", String.valueOf(ratingBar_string3));
                 */
                i.putExtra("scoreText_string",scoreText_string);
                startActivity(i);
            }
        });
    }
}
