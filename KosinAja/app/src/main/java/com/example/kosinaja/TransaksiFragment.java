package com.example.kosinaja;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;


public class TransaksiFragment extends Fragment {

    RecyclerView recyclerView;
    private ArrayList<Model> arrayList;
    FirebaseDatabase mfirebaseDatabase;
    DatabaseReference mRef;
    ProgressDialog progressDialog;
    SharedPreferences mSharePreferences;
    LinearLayoutManager mLayoutManager;
    ProgressDialog progressDialog2;
    PesanViewHolder holder;
    private final String CHANNEL_ID="pERSONAL NOTIFICATION";
    private final int NOTIFICATION_ID=001;
    public static final String MyPREFERENCES = "MyPrefs" ;
    public static final String Name = "nameKey";
    public static final String Phone = "phoneKey";
    public static final String Email = "emailKey";
    SharedPreferences sharedpreferences;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v= inflater.inflate(R.layout.fragment_transaksi, container, false);

        recyclerView= v.findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        arrayList= new ArrayList<Model>();
        mfirebaseDatabase = FirebaseDatabase.getInstance();
        mRef = mfirebaseDatabase.getReference("pesan");
        mLayoutManager=new LinearLayoutManager(getActivity());

//        ActionBar actionBar =  ((AppCompatActivity) getActivity()).getSupportActionBar();
//        actionBar.setTitle("Data Transaksi");
//        actionBar.setDisplayHomeAsUpEnabled(true);
//        actionBar.setDisplayShowHomeEnabled(true);

        sharedpreferences = this.getActivity().getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);



        return v;
    }

    @Override
    public void onStart(){
        super.onStart();
        FirebaseRecyclerAdapter<Model, PesanViewHolder> firebaseRecyclerAdapter = new FirebaseRecyclerAdapter<Model, PesanViewHolder>(
                Model.class,
                R.layout.tabel,
                PesanViewHolder.class,
                mRef
        ) {
            @Override
            protected void populateViewHolder(final PesanViewHolder holder, Model model, int i) {
                holder.setPesan(getActivity(), model.getOrderId(), model.getTitlle(), model.getHarga());


                holder.txt_bayar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {


                        progressDialog = new ProgressDialog(getActivity());
                        progressDialog.show();
                        progressDialog.setContentView(R.layout.progressdialog);
                        progressDialog.getWindow().setBackgroundDrawableResource(
                                android.R.color.transparent
                        );
                        createNotificationChannel();
                        notif();


                        holder.txt_status.setText("Pembayaran success");
                        holder.txt_status.setBackgroundResource(R.color.hijau);
                        holder.txt_cancel.setText("");
                        holder.txt_bayar.setText("");
                    }
                });


                holder.txt_cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(getActivity(), R.style.myDialog));
                        builder.setCancelable(true);
                        builder.setTitle("Batalkan Pemesanan");
                        builder.setMessage("Apakah anda yakin untuk membatalkan pesanan ini?");
                        builder.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                progressDialog2 = new ProgressDialog(getActivity());
                                progressDialog2.show();
                                progressDialog2.setContentView(R.layout.dialogcancel);
                                progressDialog2.getWindow().setBackgroundDrawableResource(
                                        android.R.color.transparent
                                );

                                createNotificationChannel();
                                notifCancel();
                                holder.txt_status.setText("Dibatalkan");
                                holder.txt_status.setBackgroundResource(R.color.merah);
                                holder.txt_cancel.setText("");
                                holder.txt_bayar.setText("");

                            }
                        });
                        builder.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Toast.makeText(getActivity(), "Selesaikan pembayaran", Toast.LENGTH_SHORT).show();
                                dialog.dismiss();
                            }
                        });
                        builder.show();


                    }
                });
            }

        };
        recyclerView.setAdapter(firebaseRecyclerAdapter);
    }

//    @Override
//    public void onBackPressed() {
//        progressDialog.dismiss();
//        progressDialog2.dismiss();
//    }

    private void createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = "PERSONAL NOTIFICATION";
            String description ="Include all the personal notifications ";
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, name, importance);
            channel.setDescription(description);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = getActivity().getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }

//    @Override
//    public boolean onSupportNavigateUp() {
//        onBackPressed();
//        return true;
//    }

    public void notif(){
        String message = "Terima kasih, pembayaran anda berhasil silahkan melakukan konfirmasi pada aplikasi";
        NotificationCompat.Builder notif = new NotificationCompat.Builder(
                getActivity(), CHANNEL_ID
        )
                .setSmallIcon(R.drawable.ic_message)
                .setContentTitle("Notifikasi Pembayaran KosinAaja")
                .setContentText(message)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setAutoCancel(true);
        Intent intent =new Intent (getActivity(), HomeFragment.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        PendingIntent pendingIntent= PendingIntent.getActivity(getActivity(), 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        notif.setContentIntent(pendingIntent);

        NotificationManagerCompat notificationManagerCompat = NotificationManagerCompat.from(getActivity());
        notificationManagerCompat.notify(NOTIFICATION_ID, notif.build());
    }
    public void notifCancel(){
        String message = "Pemesanan kost anda sudah dibatalkan";
        NotificationCompat.Builder notif = new NotificationCompat.Builder(
                getActivity(), CHANNEL_ID
        )
                .setSmallIcon(R.drawable.ic_message)
                .setContentTitle("Notifikasi Pembayaran KosinAaja")
                .setContentText(message)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setAutoCancel(true);
        Intent intent =new Intent (getActivity(), HomeFragment.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        PendingIntent pendingIntent= PendingIntent.getActivity(getActivity(), 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        notif.setContentIntent(pendingIntent);

        NotificationManagerCompat notificationManagerCompat = NotificationManagerCompat.from(getActivity());
        notificationManagerCompat.notify(NOTIFICATION_ID, notif.build());
    }

}