package com.example.kosinaja;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.midtrans.sdk.corekit.callback.TransactionFinishedCallback;
import com.midtrans.sdk.corekit.core.MidtransSDK;
import com.midtrans.sdk.corekit.core.TransactionRequest;
import com.midtrans.sdk.corekit.core.themes.CustomColorTheme;
import com.midtrans.sdk.corekit.models.CustomerDetails;
import com.midtrans.sdk.corekit.models.ItemDetails;
import com.midtrans.sdk.corekit.models.snap.CreditCard;
import com.midtrans.sdk.corekit.models.snap.TransactionResult;
import com.midtrans.sdk.uikit.SdkUIFlowBuilder;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import static com.example.kosinaja.Pesan.transactionRequest;

public class pesankost3 extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemSelectedListener, TransactionFinishedCallback {

    FirebaseStorage storage;
    StorageReference storageReference;
    FirebaseAuth mAuth;
    DatabaseReference db;
    FirebaseUser user;

    int PICK_IMAGE_REQUEST = 71;
    Uri filepath;
    public Uri donwload;

    Uri imageUri;
    Button pesanKost_btn, next_btn;
    ImageButton uploadImg_btn;
    TextView tv_path;

    public String id;
    public String jenisID;
    public String nama, noHP, pekerjaan, gender, namaUniv, tglMasuk, satuanSewa, durasiSewa, deskripsi, photo_id_str;
    public Bitmap photo_id;

    ProgressDialog loading;

    String[] jenisIDarray = {"e-KTP", "SIM", "Paspor"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pesankost3);
        initMidtransSdk();

        //get string variables from previous activity
        Intent intent = getIntent();
        nama = intent.getStringExtra("nama");
        noHP = intent.getStringExtra("noHP");
        pekerjaan = intent.getStringExtra("pekerjaan");
        gender = intent.getStringExtra("gender");
        namaUniv = intent.getStringExtra("namaUniv");
        tglMasuk = intent.getStringExtra("tglMasuk");
        satuanSewa = intent.getStringExtra("satuanSewa");
        durasiSewa = intent.getStringExtra("durasiSewa");
        deskripsi = intent.getStringExtra("deskripsi");

        //declare ProcessDialog object
        loading = new ProgressDialog(this);

        //declare Firebase objects
        mAuth = FirebaseAuth.getInstance();
        db = FirebaseDatabase.getInstance().getReference("pesankost");
        user = mAuth.getCurrentUser();

        //declare xml components
        uploadImg_btn = findViewById(R.id.uploadImg_btn);
        tv_path = findViewById(R.id.path_tv);
        pesanKost_btn = findViewById(R.id.pesanKost_btn);
        next_btn = findViewById(R.id.next_btn);
        Spinner spinner_jenisID = (Spinner) findViewById(R.id.spinner_jenisID);

        //jenisID spinner adapter
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, jenisIDarray);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_jenisID.setAdapter(adapter);
        spinner_jenisID.setOnItemSelectedListener(this);

        //set method for buttons
        pesanKost_btn.setOnClickListener(this);
        uploadImg_btn.setOnClickListener(this);
    }

    //onClick method for buttons
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.pesanKost_btn:
                uploadImage();
                loading.setTitle("Uploading....");
                loading.show();
                break;
            case R.id.uploadImg_btn:
                Intent galery = new Intent();
                galery.setType("image/*");
                galery.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(galery,"Select Photo"),PICK_IMAGE_REQUEST);
                break;
        }
    }

    //button lanjutkan ke pembayaran
    public void lanjut(View view) {
//        Intent intent = new Intent(getApplicationContext(), ActivityPembayaran.class);
//        intent.putExtra("namaPemesan", nama);
//        intent.putExtra("idPemesan", id);
//        startActivity(intent);
        actionButton();
    }

    private void actionButton() {
        MidtransSDK.getInstance().setTransactionRequest(transactionRequest("101",650000, 1, "susus murni"));
        MidtransSDK.getInstance().startPaymentUiFlow(this);
    }
    public static CustomerDetails customerDetails(){
        CustomerDetails cd = new CustomerDetails();
        cd.setFirstName("YOUR_PRODUCT");
        cd.setEmail("your_email@gmail.com");
        cd.setPhone("your_phone");
        return cd;
    }
    public static TransactionRequest transactionRequest(String id, int price, int qty, String name){
        TransactionRequest request =  new TransactionRequest(System.currentTimeMillis() + " " , 650000);
        request.setCustomerDetails(customerDetails());
        ItemDetails details = new ItemDetails(id, price, qty, name);

        ArrayList<ItemDetails> itemDetails = new ArrayList<>();
        itemDetails.add(details);
        request.setItemDetails(itemDetails);
        CreditCard creditCard = new CreditCard();
        creditCard.setSaveCard(false);
        creditCard.setAuthentication(CreditCard.AUTHENTICATION_TYPE_RBA);

        request.setCreditCard(creditCard);
        return request;
    }

    //get selected jenis ID
    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        Spinner spinner_jenisID = (Spinner) findViewById(R.id.spinner_jenisID);
        jenisID = spinner_jenisID.getItemAtPosition(position).toString();
    }

    //if no jenis ID is selected
    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    //upload data to firebase
    public  void uploadImage(){

        storage = FirebaseStorage.getInstance();
        storageReference = storage.getReference();

        if (filepath != null) {
            String filename = "images/" + user.getEmail() + db.push().getKey();
            StorageReference ref = storageReference.child(filename);
            ref.putFile(filepath).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    Task<Uri> uriTask = taskSnapshot.getStorage().getDownloadUrl();
                    while (!uriTask.isSuccessful()) ;
                    donwload = uriTask.getResult();
                    id = db.push().getKey();
                    HashMap<Object, String> data = new HashMap<>();
                    data.put("id", id);
                    data.put("nama", nama);
                    data.put("noHP", noHP);
                    data.put("pekerjaan", pekerjaan);
                    data.put("namaUniv", namaUniv);
                    data.put("tglMasuk", tglMasuk);
                    data.put("satuanSewa", satuanSewa);
                    data.put("durasiSewa", durasiSewa);
                    data.put("deskripsi", deskripsi);
                    data.put("jenisID", jenisID);
                    data.put("imageIDcard", donwload.toString());
                    db.child(id).setValue(data);
                    Toast.makeText(pesankost3.this, "Berhasil pesan kost!\nSilahkan lanjutkan ke pembayaran.",
                            Toast.LENGTH_LONG).show();
                    loading.dismiss();
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                }
            });
        }else{
            Toast.makeText(this, "error upload foto", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_IMAGE_REQUEST && RESULT_OK == resultCode&& data != null && data.getData() != null){
            filepath = data.getData();
            try {
                Bitmap photo = MediaStore.Images.Media.getBitmap(getContentResolver(), filepath);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }
    private void initMidtransSdk() {
        SdkUIFlowBuilder.init()
                .setContext(this)
                .setMerchantBaseUrl(BuildConfig.BASE_URL)
                .setClientKey(BuildConfig.CLIENT_KEY)
                .setTransactionFinishedCallback(this)
                .enableLog(true)
                .setColorTheme(new CustomColorTheme("#FFE51255", "#B61548", "#FFE51255"))
                .buildSDK();
    }

    @Override
    public void onTransactionFinished(TransactionResult result) {
        if (result.getResponse() != null) {
            switch (result.getStatus()) {
                case TransactionResult.STATUS_SUCCESS:
                    Toast.makeText(this, "Transaksi Selesai ID " + result.getResponse().getTransactionId(), Toast.LENGTH_SHORT).show();
                    break;
                case TransactionResult.STATUS_PENDING:
                    Toast.makeText(this, "Transaksi Pending ID " + result.getResponse().getTransactionId(), Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(this, PembayaranSukses.class);
                    startActivity(intent);
                    break;
                case TransactionResult.STATUS_FAILED:
                    Toast.makeText(this, "Transaksi Gagal ID " + result.getResponse().getTransactionId(), Toast.LENGTH_SHORT).show();
                    break;
            }
            result.getResponse().getValidationMessages();

        } else if (result.isTransactionCanceled()) {
            Toast.makeText(this, "Transaksi dibatalkan", Toast.LENGTH_SHORT).show();
        } else {
            if (result.getStatus().equalsIgnoreCase(TransactionResult.STATUS_INVALID)) {
                Toast.makeText(this, "Transaksi Invalid", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(this, "Transaksi finished with failure", Toast.LENGTH_SHORT).show();
            }

        }
    }

    }

