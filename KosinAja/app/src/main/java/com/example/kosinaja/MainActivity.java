package com.example.kosinaja;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.content.Context;


import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class MainActivity extends AppCompatActivity {

    private FirebaseAuth mAuth;
    EditText emaillog, passwordlog;
    private Button btnLogin;
    private TextView btnRegister;
    private String email, password;
    private static final String TAG = "MainActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Initialize Firebase Auth
        mAuth = FirebaseAuth.getInstance();

        emaillog = findViewById(R.id.emailogin_edittext);
        passwordlog = findViewById(R.id.password_edittext);

        btnLogin = findViewById(R.id.button);
        btnRegister = findViewById(R.id.register_textview);


    }
        public void login (View view) {
            email = emaillog.getText().toString();
            password = passwordlog.getText().toString();
           if (email.equals("")) {
                Toast.makeText(MainActivity.this, "Email Required", Toast.LENGTH_SHORT).show();
            } else if (password.equals("")) {
                Toast.makeText(MainActivity.this, "Password Required", Toast.LENGTH_SHORT).show();
            } else {
            mAuth.signInWithEmailAndPassword(email, password)
                    .addOnCompleteListener(MainActivity.this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {
                                // Sign in success, update UI with the signed-in user's information
                                Log.d(TAG, "signInWithEmail:success");
                                FirebaseUser user = mAuth.getCurrentUser();
                                startActivity(new Intent(getApplicationContext(), HomeScreen.class));
                                Toast.makeText(MainActivity.this, "Authentication Succes.", Toast.LENGTH_SHORT).show();
                            } else {
                                // If sign in fails, display a message to the user.
                                Log.w(TAG, "signInWithEmail:failure", task.getException());
                                Toast.makeText(getApplicationContext(), "Authentication failed.",
                                        Toast.LENGTH_SHORT).show();
                            }

                            // ...
                        }
                    });
        }
    }

    public void register (View view) {
        Intent regist = new Intent(MainActivity.this,RegisterActivity.class);
        startActivity(regist);
    }


    //idel bikin ini doang
    public void forumjualbeli(View view) {
        Intent fjb = new Intent(getApplicationContext(),Activity_Feed.class);
        startActivity(fjb);
    }
    public void reset (View view) {
        Intent reset = new Intent(MainActivity.this,ProfileActivity.class);
        startActivity(reset);
    }

}