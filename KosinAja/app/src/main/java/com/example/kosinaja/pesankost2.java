package com.example.kosinaja;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;

import android.content.Intent;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class pesankost2 extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    EditText et_durasiSewa, et_deskripsi;
    Button btn_pickDate, btn_lanjut;
    public String durasiSewa, deskripsi, satuanSewa, tglMasuk;
    TextView tv_tanggal;

    public String nama, noHP, pekerjaan, gender, namaUniv;

    String[] sewaArray = {"Tahunan", "Bulanan"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pesankost2);

        //get string variables from previous activity
        Intent intent = getIntent();
        nama = intent.getStringExtra("nama");
        noHP = intent.getStringExtra("noHP");
        pekerjaan = intent.getStringExtra("pekerjaan");
        gender = intent.getStringExtra("gender");
        namaUniv = intent.getStringExtra("namaUniv");

        //declare xml components
        btn_pickDate = findViewById(R.id.btn_pickDate);
        et_durasiSewa = findViewById(R.id.durasi_input);
        et_deskripsi = findViewById(R.id.deskripsi_input);
        btn_lanjut = findViewById(R.id.btn_next);
        tv_tanggal = findViewById(R.id.tv_tanggal);
        Spinner spinner_sewa = (Spinner) findViewById(R.id.spinner_sewa);

        //gender spinner adapter
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, sewaArray);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_sewa.setAdapter(adapter);
        spinner_sewa.setOnItemSelectedListener(this);
    }

    //get selected hitungan sewa
    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        Spinner spinner_sewa = (Spinner) findViewById(R.id.spinner_sewa);
        satuanSewa = spinner_sewa.getItemAtPosition(position).toString();
    }

    //if no hitungan sewa selected
    @Override
    public void onNothingSelected(AdapterView<?> parent) {
        Toast.makeText(this, "Pilih salah satu satuan sewa", Toast.LENGTH_SHORT).show();
    }

    //datepicker fragment
    public void showDatePicker(View view) {
        DialogFragment newFragment = new DatePickerFragment();
        newFragment.show(getSupportFragmentManager(), "datePicker");
    }

    //get selected date and put into textview
    public void processDatePickerResult(int year, int month, int day) {
        String month_string = Integer.toString(month + 1);
        String day_string = Integer.toString(day);
        String year_string = Integer.toString(year);
        tglMasuk = (month_string + "/" + day_string + "/" + year_string);
        tv_tanggal.setText(tglMasuk);
    }


    public void lanjut(View view) {

        //validation
        if (TextUtils.isEmpty(et_durasiSewa.getText()) || TextUtils.isEmpty(et_deskripsi.getText())
                || TextUtils.isEmpty(tv_tanggal.getText())
                || satuanSewa.equals("")) {
            Toast.makeText(this, "Isi semua baris yang diperlukan", Toast.LENGTH_SHORT).show();
        }
        else {
            //declare variables
            durasiSewa = et_durasiSewa.getText().toString();
            deskripsi = et_deskripsi.getText().toString();
            Intent intent = new Intent(getApplicationContext(), pesankost3.class);
            intent.putExtra("nama", nama);
            intent.putExtra("noHP", noHP);
            intent.putExtra("pekerjaan", pekerjaan);
            intent.putExtra("namaUniv", namaUniv);
            intent.putExtra("gender", gender);
            intent.putExtra("tglMasuk", tglMasuk);
            intent.putExtra("satuanSewa", satuanSewa);
            intent.putExtra("durasiSewa", durasiSewa);
            intent.putExtra("deskripsi", deskripsi);
            startActivity(intent);
        }

    }

}
