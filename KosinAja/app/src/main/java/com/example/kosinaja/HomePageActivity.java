package com.example.kosinaja;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;
import android.widget.TextView;


import com.example.kosinaja.Helper.LocaleHelper;

import io.paperdb.Paper;
public class HomePageActivity extends AppCompatActivity implements View.OnClickListener {
    ImageView imgreview;
    LinearLayout carikost;
    LinearLayout  homeinput;
    TextView mProfile, mEditProfile, mInputKost, mInputHere, mForum, mForumHere, mSearch, mSearchhere, mReview, mReviewhere;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_page);

        homeinput = findViewById(R.id.home_kost);
        homeinput.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent home = new Intent(getApplicationContext(), homeInput.class);
                startActivity(home);
            }
        });

        mProfile = (TextView) findViewById(R.id.profilee);

        mEditProfile = (TextView) findViewById(R.id.edit_prof);

        mInputKost = (TextView) findViewById(R.id.inputkost);

        mInputHere = (TextView) findViewById(R.id.input_here);

        mForum = (TextView) findViewById(R.id.forumm);

        mForumHere = (TextView) findViewById(R.id.forum_here);

        mSearch = (TextView) findViewById(R.id.searchh);

        mSearchhere = (TextView) findViewById(R.id.search_here);

        mReview = (TextView) findViewById(R.id.revieww);

        mReviewhere = (TextView) findViewById(R.id.review_here);


        Paper.init(this);


        String language = Paper.book().read("language");
        if (language == null)
            Paper.book().write("language", "en");

        updateView((String) Paper.book().read("language"));


        carikost=findViewById(R.id.carikost);
        carikost.setOnClickListener(this);

        imgreview = findViewById(R.id.imgreview);

        imgreview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(HomePageActivity.this,score.class);
                startActivity(i);
            }
        });


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
            getMenuInflater().inflate(R.menu.menu_main, menu);
            int nightMode = AppCompatDelegate.getDefaultNightMode();
            if(nightMode == AppCompatDelegate.MODE_NIGHT_NO){
                menu.findItem(R.id.night_mode).setTitle(R.string.night_mode);
            } else{
                menu.findItem(R.id.night_mode).setTitle(R.string.day_mode);
            }
            return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        int id = item.getItemId();

//        if (id == R.id.menu_chart) {
//            Intent intent = new Intent(HomePageActivity.this, DataTransaksi.class);
//            startActivity(intent);
        if (item.getItemId() == R.id.night_mode) {
            int nightMode = AppCompatDelegate.getDefaultNightMode();
        if (nightMode == AppCompatDelegate.MODE_NIGHT_NO) {
            AppCompatDelegate.setDefaultNightMode
                    (AppCompatDelegate.MODE_NIGHT_YES);
        }else  if (item.getItemId() == R.id.language_indonesia) {
                        Paper.book().write("language","in");
                        updateView((String) Paper.book().read("language"));
        } else if (item.getItemId() == R.id.language_english) {
                        Paper.book().write("language","en");
                        updateView((String) Paper.book().read("language"));

                } else {
                    AppCompatDelegate.setDefaultNightMode
                            (AppCompatDelegate.MODE_NIGHT_NO);

                }
                recreate();

            }
        return true;
    }

    @Override
    public void onClick(View v) {
      switch (v.getId()){
         case R.id.carikost:
            Intent intent = new Intent(HomePageActivity.this,KostList.class);
             startActivity(intent);
           break;
     }

    }


//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        if(item.getItemId()==R.id.night_mode) {
//            int nightMode = AppCompatDelegate.getDefaultNightMode();
//            if (nightMode == AppCompatDelegate.MODE_NIGHT_NO) {
//                AppCompatDelegate.setDefaultNightMode
//                        (AppCompatDelegate.MODE_NIGHT_YES);
//            } else {
//                AppCompatDelegate.setDefaultNightMode
//                        (AppCompatDelegate.MODE_NIGHT_NO);
//            }
//            recreate();
//        }
//            return true;
//        }




    private void updateView(String lang) {
        Context context = LocaleHelper.setLocale(this,lang);
        Resources resources = context.getResources();

        mProfile.setText(resources.getString(R.string.Profile));
        mEditProfile.setText(resources.getString(R.string.Edit_Profile));

        mInputKost.setText(resources.getString(R.string.Input_Kost));
        mInputHere.setText(resources.getString(R.string.Input_here));

        mForum.setText(resources.getString(R.string.forum));
        mForumHere.setText(resources.getString(R.string.Discuss_Here));

        mSearch.setText(resources.getString(R.string.Search));
        mSearchhere.setText(resources.getString(R.string.Search_Here));

        mReview.setText(resources.getString(R.string.Review));
        mReviewhere.setText(resources.getString(R.string.Your_Review));

    }



//    @Override
//    public boolean onOptionsItemSelected( MenuItem item) {
//
//        if (item.getItemId() == R.id.language_indonesia)
//        {
//            Paper.book().write("language","in");
//            updateView((String) Paper.book().read("language"));
//
//        }
//        else if (item.getItemId() == R.id.language_english)
//        {
//            Paper.book().write("language","en");
//            updateView((String) Paper.book().read("language"));
//
//        }
//        return true;
//    }



    public void InputKos(View view) {
        startActivity(new Intent(HomePageActivity.this, homeInput.class));
    }

    public void forumjualbeli(View view) {
        Intent fjb = new Intent(getApplicationContext(),Activity_Feed.class);
        startActivity(fjb);
    }

    public void ProfilUser(View view) {
        Intent profil = new Intent(getApplicationContext(),ProfileActivity.class);
        startActivity(profil);
    }
}

