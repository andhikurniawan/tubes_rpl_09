package com.example.kosinaja;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;
import com.squareup.picasso.Picasso;

public class CustomInfoWindow implements GoogleMap.InfoWindowAdapter {
    private Context context;

    public CustomInfoWindow(Context ctx){
        context = ctx;
    }
    @Override
    public View getInfoWindow(Marker marker) {
        return null;
    }

    @Override
    public View getInfoContents(Marker marker) {
        View view = ((Activity)context).getLayoutInflater()
                .inflate(R.layout.info_window, null);

        TextView title = view.findViewById(R.id.titleWindow);
        TextView deskripsi = view.findViewById(R.id.deskripsiWindow);
        TextView harga = view.findViewById(R.id.hargaWindow);
        ImageView img = view.findViewById(R.id.imageWindow);
        TextView lat=view.findViewById(R.id.Latitude);
        TextView lng=view.findViewById(R.id.Longitude);

        title.setText(marker.getTitle());
        deskripsi.setText(marker.getSnippet());

        InfoWindowData infoWindowData = (InfoWindowData) marker.getTag();

//        int imageId = context.getResources().getIdentifier(infoWindowData.getImage().toLowerCase(),
//                "drawable", context.getPackageName());
//        img.setImageResource(imageId);

        Picasso.get()
                .load(infoWindowData.getImage())
                .into(img);

        harga.setText(infoWindowData.getHarga());
        lat.setText(infoWindowData.getLatitude());
        lng.setText(infoWindowData.getLongitude());

        return view;
    }
}
