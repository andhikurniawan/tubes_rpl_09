package com.example.kosinaja;

import com.midtrans.sdk.corekit.core.TransactionRequest;
import com.midtrans.sdk.corekit.models.BankType;
import com.midtrans.sdk.corekit.models.CustomerDetails;
import com.midtrans.sdk.corekit.models.ItemDetails;
import com.midtrans.sdk.corekit.models.snap.CreditCard;

import java.util.ArrayList;

public class Model {
 private String email, titlle, harga, sWaktu, sType, orderId;

public Model(){
}

    public Model(String email, String titlle, String harga, String sWaktu, String sType, String orderId) {
        this.email = email;
        this.titlle = titlle;
        this.harga = harga;
        this.sWaktu = sWaktu;
        this.sType = sType;
        this.orderId=orderId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTitlle() {
        return titlle;
    }

    public void setTitlle(String titlle) {
        this.titlle = titlle;
    }

    public String getHarga() {
        return harga;
    }

    public void setHarga(String harga) {
        this.harga = harga;
    }

    public String getsWaktu() {
        return sWaktu;
    }

    public void setsWaktu(String sWaktu) {
        this.sWaktu = sWaktu;
    }

    public String getsType() {
        return sType;
    }

    public void setsType(String sType) {
        this.sType = sType;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }
}
