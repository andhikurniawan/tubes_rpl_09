package com.example.kosinaja;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.HashMap;

public class score extends AppCompatActivity {
    FirebaseStorage storage;
    StorageReference storageReference;
    FirebaseAuth mAuth;
    DatabaseReference db;
    FirebaseUser user;
    EditText scoreText;
    public String scoreText_string;
    ProgressDialog silahkantunggu;

    private Button review_next;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_score);

        mAuth = FirebaseAuth.getInstance();
        db = FirebaseDatabase.getInstance().getReference("reviewkost");
        user = mAuth.getCurrentUser();

        scoreText= findViewById(R.id.scoreText);

        review_next= findViewById(R.id.review_next);

        review_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String scorekosan = db.push().getKey();
                String scoreText_string = scoreText.getText().toString();

                HashMap<Object, String> data = new HashMap<>();
                data.put("Score Kosan", String.valueOf(scoreText_string));
                //db.child(scorekosan).setValue(data);

                if (TextUtils.isEmpty(scoreText.getText())) {
                    scoreText.setError("Silahkan masukkan score");
                    scoreText.requestFocus();
                }
                else {
                    silahkantunggu = ProgressDialog.show(score.this, null,"please wait", true, false);
                    Intent i = new Intent(score.this, review.class);
                    i.putExtra("scoreText_string",scoreText_string);
                    startActivity(i);
                }

            }
        });
    }

}
