package com.example.kosinaja;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.w3c.dom.Comment;

import java.util.ArrayList;
import java.util.Random;

public class
Activity_Feed extends AppCompatActivity {

    RecyclerView recyclerView;
    RecyclerView commentRV;
    CommentAdapter commentAdapter;
    ArrayList<ModelComment> modelCommentArrayList = new ArrayList<>();
    ArrayList<ModelFeed> modelFeedArrayList = new ArrayList<>();
    AdapterFeed adapterFeed;

    DatabaseReference db ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feed);

        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        adapterFeed = new AdapterFeed(this, modelFeedArrayList);
        recyclerView.setAdapter(adapterFeed);

        db = FirebaseDatabase.getInstance().getReference("fjbPosts");

        populateRecyclerView();
    }

    public void populateRecyclerView() {

        db.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                modelFeedArrayList.clear();
                for (DataSnapshot ds : dataSnapshot.getChildren()){
                    String id = ds.child("id").getValue().toString();
                    String email = ds.child("email").getValue().toString();
                    String post = ds.child("post").getValue().toString();
                    String image= ds.child("image").getValue().toString();
                    if (image == ""){
                        image = "R.drawable.ic_profile1";
                    }
                    ModelFeed mf = new ModelFeed(id,email,post,image);
                    modelFeedArrayList.add(mf);
                }
                adapterFeed.notifyDataSetChanged();
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });
    }

    public void addPost(View view) {
        Intent post = new Intent(this,AddPostActivity.class);
        startActivity(post);
    }
}