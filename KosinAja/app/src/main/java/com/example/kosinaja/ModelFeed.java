package com.example.kosinaja;

public class ModelFeed {


    String id,email,post,image;

    public ModelFeed(String id, String email, String post, String image) {
        this.id = id;
        this.email = email;
        this.post = post;
        this.image = image;
    }

    public String getEmail() {
        return email;
    }

    public String getId() {
        return id;
    }

    public String getPost() {
        return post;
    }

    public String getImage() {
        return image;
    }
}
