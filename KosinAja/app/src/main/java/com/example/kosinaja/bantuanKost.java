package com.example.kosinaja;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class bantuanKost extends AppCompatActivity {
    Button Understand;
    TextView Helper1, Helper2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bantuan_kost);

        Understand = findViewById(R.id.btn_understand);
        Helper1 = findViewById(R.id.helper1);
        Helper2 = findViewById(R.id.helper2);

        Understand.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent uds = new Intent(getApplicationContext(),kostBaru.class);
                startActivity(uds);
            }
        });
        Helper1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent hp1 = new Intent(getApplicationContext(),helper_2.class);
                startActivity(hp1);
            }
        });

        Helper2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent hp2 = new Intent(getApplicationContext(),helper1.class);
                startActivity(hp2);
            }
        });
    }
}
