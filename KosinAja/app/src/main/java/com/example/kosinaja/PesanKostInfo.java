package com.example.kosinaja;

public class PesanKostInfo {
    String nama, jenisKelamin, noHP, pekerjaan, namaUniv, tglMasuk, deskripsi, satuanSewa;
    int durasiSewa;

    public PesanKostInfo() {

    }

    public PesanKostInfo(String nama, String jenisKelamin, String noHP, String pekerjaan, String namaUniv,
                         String tglMasuk, String deskripsi, String hitunganSewa, int hitSewa, int durasiSewa) {
        this.nama = nama;
        this.jenisKelamin = jenisKelamin;
        this.noHP = noHP;
        this.pekerjaan = pekerjaan;
        this.namaUniv = namaUniv;
        this.tglMasuk = tglMasuk;
        this.deskripsi = deskripsi;
        this.satuanSewa = hitunganSewa;
        this.durasiSewa = durasiSewa;
    }

    public String getNama() {
        return nama;
    }

    public String getJenisKelamin() {
        return jenisKelamin;
    }

    public String getNoHP() {
        return noHP;
    }

    public String getPekerjaan() {
        return pekerjaan;
    }

    public String getNamaUniv() {
        return namaUniv;
    }

    public String getTglMasuk() {
        return tglMasuk;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public String getSatuanSewa() {
        return satuanSewa;
    }

    public int getDurasiSewa() {
        return durasiSewa;
    }
}
