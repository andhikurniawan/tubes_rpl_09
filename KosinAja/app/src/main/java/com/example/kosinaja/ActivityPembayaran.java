package com.example.kosinaja;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;

public class ActivityPembayaran extends AppCompatActivity implements AdapterView.OnItemSelectedListener, View.OnClickListener {

    FirebaseStorage storage;
    StorageReference storageReference;
    FirebaseAuth mAuth;
    DatabaseReference db;
    FirebaseUser user;

    int PICK_IMAGE_REQUEST = 71;
    Uri filepath;
    public Uri donwload;

    public String namaPemesan, idPemesan;

    public String selectedMethod;
    Button btn_submit;
    ImageButton btn_upload;
    TextView tv_VANumber, tv_path;
    private static final int PICK_IMAGE = 1;
    Uri imageUri;
    ProgressDialog loading;
    String[] metodeBayar = {"BCA", "Mandiri", "BNI"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pembayaran);

        Intent intent = getIntent();
        namaPemesan = intent.getStringExtra("namaPemesan");
        idPemesan = intent.getStringExtra("idPemesan");

        btn_upload = findViewById(R.id.btn_upload);
        tv_VANumber = findViewById(R.id.tv_virtualAccNumber);
        tv_path = findViewById(R.id.tv_path);
        btn_submit = findViewById(R.id.btn_submit);

        Spinner spinner1 = (Spinner) findViewById(R.id.spinner1);
        loading = new ProgressDialog(this);

        mAuth = FirebaseAuth.getInstance();
        db = FirebaseDatabase.getInstance().getReference("pembayaran");
        user = mAuth.getCurrentUser();

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, metodeBayar);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner1.setAdapter(adapter);
        spinner1.setOnItemSelectedListener(this);

        btn_submit.setOnClickListener(this);
        btn_upload.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_submit:
                uploadImage();
                loading.setTitle("Uploading....");
                loading.show();
                Intent intent = new Intent(getApplicationContext(), PembayaranSukses.class);
                startActivity(intent);
                break;
            case R.id.btn_upload:
                Intent galery = new Intent();
                galery.setType("image/*");
                galery.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(galery,"Select Photo"),PICK_IMAGE_REQUEST);
                break;
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> arg0, View arg1, int position,long id) {
        Spinner spinner1 = (Spinner) findViewById(R.id.spinner1);
        selectedMethod = spinner1.getItemAtPosition(position).toString();
        if (selectedMethod.equals("BCA")) {
            tv_VANumber.setText("1202174372");
        }
        else if (selectedMethod.equals("Mandiri")) {
            tv_VANumber.setText("9993253266");
        }
        else if (selectedMethod.equals("BNI")) {
            tv_VANumber.setText("0300517432");
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> arg0) {
        // TODO - Custom Code
    }

    public  void uploadImage(){

        storage = FirebaseStorage.getInstance();
        storageReference = storage.getReference();

        if (filepath != null) {
            String filename = "images/" + user.getEmail() + db.push().getKey();
            StorageReference ref = storageReference.child(filename);
            ref.putFile(filepath).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    Task<Uri> uriTask = taskSnapshot.getStorage().getDownloadUrl();
                    while (!uriTask.isSuccessful()) ;
                    donwload = uriTask.getResult();
                    String id = db.push().getKey();
                    HashMap<Object, String> data = new HashMap<>();
                    data.put("idPembayaran", id);
                    data.put("metodeBayar", selectedMethod);
                    data.put("buktiBayar", donwload.toString());
                    data.put("namaPemesan", namaPemesan);
                    data.put("idPemesan", idPemesan);
                    db.child(id).setValue(data);
                    Toast.makeText(ActivityPembayaran.this, "Berhasil", Toast.LENGTH_SHORT).show();
                    loading.dismiss();
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                }
            });
        }else{
            Toast.makeText(this, "Error upload photo", Toast.LENGTH_SHORT).show();
        }
    }

    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_IMAGE_REQUEST && RESULT_OK == resultCode&& data != null && data.getData() != null){
            filepath = data.getData();
            try {
                Bitmap photo = MediaStore.Images.Media.getBitmap(getContentResolver(), filepath);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void submit(View view) {
        Intent intent = new Intent(getApplicationContext(), PembayaranSukses.class);
        startActivity(intent);
    }

}
