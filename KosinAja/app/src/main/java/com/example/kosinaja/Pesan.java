package com.example.kosinaja;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.midtrans.sdk.corekit.callback.TransactionFinishedCallback;
import com.midtrans.sdk.corekit.core.MidtransSDK;
import com.midtrans.sdk.corekit.core.TransactionRequest;
import com.midtrans.sdk.corekit.core.themes.CustomColorTheme;
import com.midtrans.sdk.corekit.models.CustomerDetails;
import com.midtrans.sdk.corekit.models.ItemDetails;
import com.midtrans.sdk.corekit.models.snap.CreditCard;
import com.midtrans.sdk.corekit.models.snap.TransactionResult;
import com.midtrans.sdk.uikit.SdkUIFlowBuilder;

import java.util.ArrayList;
import java.util.List;


public class Pesan extends AppCompatActivity implements TransactionFinishedCallback {

    TextView tWaktu;
    TextView tType;
    private Spinner spWaktu;
    private Spinner spType;
    EditText mEmail, mTitle, mHarga;
    FirebaseDatabase database;
    DatabaseReference myref;
    Model model;
    static int charga=0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pesan);

        initMidtransSdk();

//        tType = findViewById(R.id.txt_type);
//        tWaktu= findViewById(R.id.txt_waktu);
        spType= findViewById(R.id.spType);
        spWaktu=findViewById(R.id.spWaktu);

        mEmail= findViewById(R.id.edit_email);
        mTitle= findViewById(R.id.edit_title);
        mHarga= findViewById(R.id.edit_harga);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("Data Pemesan");
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowHomeEnabled(true);




        model = new Model();
        myref = FirebaseDatabase.getInstance().getReference().child("pesan");

        List<String> cType = new ArrayList<>();
        cType.add(0,"Pilih type");
        cType.add("Pria");
        cType.add("Wanita");
        cType.add("Campuran");

        ArrayAdapter<String> tAdapter;
        tAdapter= new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, cType);
        tAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spType.setAdapter(tAdapter);
//        spType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                if (parent.getItemAtPosition(position).equals("Pilih type")){
//
//                }else {
//                    tType.setText(parent.getSelectedItem().toString());
//                }
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> parent) {
//
//            }
//        });

        List<String> cWaktu = new ArrayList<>();
        cWaktu.add(0,"Pilih period");
        cWaktu.add("Bulanan");
        cWaktu.add("Tahunan");

        ArrayAdapter<String> wAdapter;
        wAdapter= new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, cWaktu);
        wAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spWaktu.setAdapter(wAdapter);
//        spWaktu.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                if (parent.getItemAtPosition(position).equals("Pilih period")){
//
//                }else {
//                    tWaktu.setText(parent.getSelectedItem().toString());
//                }
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> parent) {
//
//            }
//        });
    }

    public void addData(View view) {
        String email= mEmail.getText().toString().trim();
        String title= mTitle.getText().toString().trim();
        String harga= mHarga.getText().toString().trim();
        String waktu = spWaktu.getSelectedItem().toString().trim();
        String type = spType.getSelectedItem().toString().trim();

        model.setEmail(email);
        model.setTitlle(title);
        model.setHarga(harga);
        model.setsType(type);
        model.setsWaktu(waktu);
        model.setOrderId("12113313131");
        myref.push().setValue(model);

        Intent intent = new Intent(this, pesankost1.class);
        startActivity(intent);


    }
    private void initMidtransSdk() {
        SdkUIFlowBuilder.init()
                .setContext(this)
                .setMerchantBaseUrl(BuildConfig.BASE_URL)
                .setClientKey(BuildConfig.CLIENT_KEY)
                .setTransactionFinishedCallback(this)
                .enableLog(true)
                .setColorTheme(new CustomColorTheme("#FFE51255", "#B61548", "#FFE51255"))
                .buildSDK();
    }

    private void actionButton() {
        MidtransSDK.getInstance().setTransactionRequest(transactionRequest("101",650000, 1, "susus murni"));
        MidtransSDK.getInstance().startPaymentUiFlow(this);
    }
    public static CustomerDetails customerDetails(){
        CustomerDetails cd = new CustomerDetails();
        cd.setFirstName("YOUR_PRODUCT");
        cd.setEmail("your_email@gmail.com");
        cd.setPhone("your_phone");
        return cd;
    }
    public static TransactionRequest transactionRequest(String id, int price, int qty, String name){
        TransactionRequest request =  new TransactionRequest(System.currentTimeMillis() + " " , 650000);
        request.setCustomerDetails(customerDetails());
        ItemDetails details = new ItemDetails(id, price, qty, name);

        ArrayList<ItemDetails> itemDetails = new ArrayList<>();
        itemDetails.add(details);
        request.setItemDetails(itemDetails);
        CreditCard creditCard = new CreditCard();
        creditCard.setSaveCard(false);
        creditCard.setAuthentication(CreditCard.AUTHENTICATION_TYPE_RBA);

        request.setCreditCard(creditCard);
        return request;
    }
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onTransactionFinished(TransactionResult result) {
        if (result.getResponse() != null) {
            switch (result.getStatus()) {
                case TransactionResult.STATUS_SUCCESS:
                    Toast.makeText(this, "Transaksi Selesai ID " + result.getResponse().getTransactionId(), Toast.LENGTH_SHORT).show();
                    break;
                case TransactionResult.STATUS_PENDING:
                    Toast.makeText(this, "Transaksi Pending ID " + result.getResponse().getTransactionId(), Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(this, HomeScreen.class);
                    startActivity(intent);
                    break;
                case TransactionResult.STATUS_FAILED:
                    Toast.makeText(this, "Transaksi Gagal ID " + result.getResponse().getTransactionId(), Toast.LENGTH_SHORT).show();
                    break;
            }
            result.getResponse().getValidationMessages();

        } else if (result.isTransactionCanceled()) {
            Toast.makeText(this, "Transaksi dibatalkan", Toast.LENGTH_SHORT).show();
        } else {
            if (result.getStatus().equalsIgnoreCase(TransactionResult.STATUS_INVALID)) {
                Toast.makeText(this, "Transaksi Invalid", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(this, "Transaksi finished with failure", Toast.LENGTH_SHORT).show();
            }

        }
    }
    }

