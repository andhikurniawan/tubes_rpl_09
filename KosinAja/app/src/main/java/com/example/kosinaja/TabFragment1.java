package com.example.kosinaja;


import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;

import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.location.LocationListener;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMapOptions;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class TabFragment1 extends Fragment implements
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener
{

    private GoogleMap mMap;
    private GoogleApiClient googleApiClient;
    private LocationRequest locationRequest;
    private Location lastLocation;
    private Marker currentUserLocationMarker;
    private static final int Request_User_Location_Code=99;
    private  double latitude, longitude;
    private int ProximityRadius= 10000;
    ArrayList<LatLng> arrayList= new ArrayList<LatLng>();
    LatLng Data1 = new LatLng(-8.643094, 115.242268);
    LatLng Data2 = new LatLng(-8.7036971, 115.1908272);
    LatLng Data3 = new LatLng(-8.6383964, 115.2393945);
    LatLng Data4 = new LatLng(-8.6455041, 115.246102);
    LatLng Data5 = new LatLng(-8.6321529, 115.247054);
    LatLng Data6 = new LatLng(-8.6305192, 115.2390393);
    LatLng Data7 = new LatLng(-8.6433883, 115.2176472);
    LatLng Data8 = new LatLng(-8.6533163,115.2205655);
    LatLng Data9 = new LatLng(-8.6495615,115.2080342);
    LatLng Data10 = new LatLng(-8.6649623,115.2139994);
    public TabFragment1() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_tab_fragment1, container, false);

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            checkUserLocationPermission();
        }
//        GoogleMapOptions options = new GoogleMapOptions()
//                .zoomControlsEnabled(true);
//        SupportMapFragment supportMapFragment = SupportMapFragment.newInstance(options);

        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                mMap = googleMap;

                mMap.getUiSettings().setZoomControlsEnabled(true);
                mMap.getUiSettings().setMapToolbarEnabled(true);

                //Data 1
                MarkerOptions markerOptions = new MarkerOptions();
                markerOptions.position(Data1)
                        .title("Amoris Kost")
                        .snippet("Jalan Gatot Subroto timur nomor 8")
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.icon_marker));

                InfoWindowData info = new InfoWindowData();
                info.setImage("https://lh5.googleusercontent.com/proxy/EIW9Q0xnqDVa4E_7U2PPl9Y-jE9YEzemnMEwkwXmsG0ENMTh3hYGIENRpMvkjgv2l9cYiV-kMXST256R543ZHaojzm4doLcDJ0vkerkm37GT7Ok8fiVqQEWEngJARs9fti-S15AB-IqwdJpbvwpCG50fQ1A6OQ=w1200-h630-p-k-no-nu");
                info.setHarga("Rp. 700.000/bulan ");
                info.setLatitude("-8.643094");
                info.setLongitude("115.242268");

                CustomInfoWindow customInfoWindow = new CustomInfoWindow(getActivity());
                mMap.setInfoWindowAdapter(customInfoWindow);

                Marker m = mMap.addMarker(markerOptions);
                m.setTag(info);
                m.showInfoWindow();

                mMap.moveCamera(CameraUpdateFactory.newLatLng(Data1));

                //Data 2
                MarkerOptions markerOptions2 = new MarkerOptions();
                markerOptions2.position(Data2)
                        .title("Kost Bougenville")
                        .snippet("Jalan Raya Sesetan nomor 2")
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.icon_marker));

                InfoWindowData info2 = new InfoWindowData();
                info2.setImage("https://azzahrafurniture.com/wp-content/uploads/2016/03/desain-kamar-kost.jpg");
                info2.setHarga("Rp. 12.500.000/tahun ");
                info2.setLatitude("-8.7036971");
                info2.setLongitude(" 115.1908272");

                CustomInfoWindow customInfoWindow2 = new CustomInfoWindow(getActivity());
                mMap.setInfoWindowAdapter(customInfoWindow2);

                Marker m2 = mMap.addMarker(markerOptions2);
                m2.setTag(info2);
                m2.showInfoWindow();

                mMap.moveCamera(CameraUpdateFactory.newLatLng(Data2));

                //Data3
                MarkerOptions markerOptions3 = new MarkerOptions();
                markerOptions3.position(Data3)
                        .title("Kost Melati Harum")
                        .snippet("Jalan Noja 1 nomor 24 kesiman")
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.icon_marker));

                InfoWindowData info3 = new InfoWindowData();
                info3.setImage("https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcT3LMfvuCe3R6wCu6vgnJUJxuyIcdBZhjJdrCdH8Y6MDZ83JZYo&usqp=CAU");
                info3.setHarga("Rp. 11.500.000/tahun");
                info3.setLatitude("-8.6383964");
                info3.setLongitude("115.2393945");

                CustomInfoWindow customInfoWindow3 = new CustomInfoWindow(getActivity());
                mMap.setInfoWindowAdapter(customInfoWindow3);

                Marker m3 = mMap.addMarker(markerOptions3);
                m3.setTag(info3);
                m3.showInfoWindow();

                mMap.moveCamera(CameraUpdateFactory.newLatLng(Data3));

                //Data4
                MarkerOptions markerOptions4 = new MarkerOptions();
                markerOptions4.position(Data4)
                        .title("Kost Kencana Putra")
                        .snippet("Jl. By Pass Ngurah Rai Gg. Kertapura VIII")
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.icon_marker));

                InfoWindowData info4 = new InfoWindowData();
                info4.setImage("https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRE3Sa1dpgGZXQpf6HC7dNvSZdfSPZj9I4GltWRQ77uCh_SXtus&usqp=CAU");
                info4.setHarga("Rp.500.000/bulan");
                info4.setLatitude("-8.6455041");
                info4.setLongitude("115.246102");

                CustomInfoWindow customInfoWindow4 = new CustomInfoWindow(getActivity());
                mMap.setInfoWindowAdapter(customInfoWindow4);

                Marker m4 = mMap.addMarker(markerOptions4);
                m4.setTag(info4);
                m4.showInfoWindow();

                mMap.moveCamera(CameraUpdateFactory.newLatLng(Data4));

                //Data5
                MarkerOptions markerOptions5= new MarkerOptions();
                markerOptions5.position(Data5)
                        .title("Kost Griya Nasti")
                        .snippet("Jl. Raya Batubulan No.63, Batubulan")
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.icon_marker));

                InfoWindowData info5 = new InfoWindowData();
                info5.setImage("https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcT_r3Mdm4Oe4Zj2T05Ne3x4Zp6uagm2h90wyGg20PnCkwYHFcVK&usqp=CAU");
                info5.setHarga("Rp.800.000/bulan");
                info5.setLatitude("-8.6321529");
                info5.setLongitude("115.247054");

                CustomInfoWindow customInfoWindow5 = new CustomInfoWindow(getActivity());
                mMap.setInfoWindowAdapter(customInfoWindow5);

                Marker m5 = mMap.addMarker(markerOptions5);
                m5.setTag(info5);
                m5.showInfoWindow();

                mMap.moveCamera(CameraUpdateFactory.newLatLng(Data5));

                //Data6
                MarkerOptions markerOptions6= new MarkerOptions();
                markerOptions6.position(Data6)
                        .title("Kost Sri Rejeki")
                        .snippet("Jl. Trengguli No.61A, Penatih")
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.icon_marker));

                InfoWindowData info6 = new InfoWindowData();
                info6.setImage("https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRzFXc9ptjuinh2IOEgvLQv8mCk5rSi_5_v-AhZN_OZIi1ihZuf&usqp=CAU");
                info6.setHarga("Rp.600.000/bulan");
                info6.setLatitude("-8.6305192");
                info6.setLongitude("115.2390393");

                CustomInfoWindow customInfoWindow6 = new CustomInfoWindow(getActivity());
                mMap.setInfoWindowAdapter(customInfoWindow6);

                Marker m6 = mMap.addMarker(markerOptions6);
                m6.setTag(info6);
                m6.showInfoWindow();

                mMap.moveCamera(CameraUpdateFactory.newLatLng(Data6));

                //Data7
                MarkerOptions markerOptions7= new MarkerOptions();
                markerOptions7.position(Data7)
                        .title("Kost Sri Kepakisan")
                        .snippet("Jl. Cokroaminoto No.35, Ubung Kaja")
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.icon_marker));

                InfoWindowData info7 = new InfoWindowData();
                info7.setImage("https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQ3RVcYYfepmFdTNK9dVdnHUkgAs3hSPPLmsFA9OUpZSlWJIh4R&usqp=CAU");
                info7.setHarga("Rp.900.000/bulan");
                info7.setLatitude("-8.6433883");
                info7.setLongitude("115.2176472");

                CustomInfoWindow customInfoWindow7 = new CustomInfoWindow(getActivity());
                mMap.setInfoWindowAdapter(customInfoWindow7);

                Marker m7 = mMap.addMarker(markerOptions7);
                m7.setTag(info7);
                m7.showInfoWindow();

                mMap.moveCamera(CameraUpdateFactory.newLatLng(Data7));

                //Data8
                MarkerOptions markerOptions8= new MarkerOptions();
                markerOptions8.position(Data8)
                        .title("Kost Bajrasandi")
                        .snippet("Jl. Raya Puputan No.142, Panjer")
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.icon_marker));

                InfoWindowData info8 = new InfoWindowData();
                info8.setImage("https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSL0U7dYOjf9ZCoLOIUDSahoWncwBkhZMUXTye-qSJC_gH8rht1&usqp=CAU");
                info8.setHarga("Rp.11.900.000/tahun");
                info8.setLatitude("-8.6533163");
                info8.setLongitude("115.2205655");

                CustomInfoWindow customInfoWindow8 = new CustomInfoWindow(getActivity());
                mMap.setInfoWindowAdapter(customInfoWindow8);

                Marker m8 = mMap.addMarker(markerOptions8);
                m8.setTag(info8);
                m8.showInfoWindow();

                mMap.moveCamera(CameraUpdateFactory.newLatLng(Data8));

                //Data9
                MarkerOptions markerOptions9= new MarkerOptions();
                markerOptions9.position(Data9)
                        .title("Kost Waja Sakti")
                        .snippet("Jl. Gunung Batukaru No.36, Pemecutan")
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.icon_marker));

                InfoWindowData info9 = new InfoWindowData();
                info9.setImage("https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcR34R1g6g2K9dj-E1gZV2r3lyNMbP7X-AEnkn76Xcfk1r1p9faB&usqp=CAU");
                info9.setHarga("Rp.10.500.000/tahun");
                info9.setLatitude("-8.6495615");
                info9.setLongitude("115.2080342");

                CustomInfoWindow customInfoWindow9 = new CustomInfoWindow(getActivity());
                mMap.setInfoWindowAdapter(customInfoWindow9);

                Marker m9 = mMap.addMarker(markerOptions9);
                m9.setTag(info9);
                m9.showInfoWindow();

                mMap.moveCamera(CameraUpdateFactory.newLatLng(Data9));

                //Data10
                MarkerOptions markerOptions10= new MarkerOptions();
                markerOptions10.position(Data10)
                        .title("Kost Dukuh Sari")
                        .snippet("Jl. Teuku Umar No.79, Dauh Puri Klod")
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.icon_marker));

                InfoWindowData info10 = new InfoWindowData();
                info10.setImage("https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTc67raVC6s-jxFOChEtThhZM7hdLKa1eJJJaLnNwasCDHCrtFx&usqp=CAU");
                info10.setHarga("Rp.650.000/bulan");
                info10.setLatitude("-8.6649623");
                info10.setLongitude("115.2139994");

                CustomInfoWindow customInfoWindow10 = new CustomInfoWindow(getActivity());
                mMap.setInfoWindowAdapter(customInfoWindow10);

                Marker m10 = mMap.addMarker(markerOptions10);
                m10.setTag(info10);
                m10.showInfoWindow();

                mMap.moveCamera(CameraUpdateFactory.newLatLng(Data10));

                //Event Infowindow
                mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
                    @Override
                    public void onInfoWindowClick(Marker marker) {
                        InfoWindowData infoAttach = ((InfoWindowData)marker.getTag());
                        String mtitle= marker.getTitle();
                        String deskripsi=marker.getSnippet();
                        String harga= infoAttach.getHarga();
                        String UrlImage = infoAttach.getImage();
                        String Lat= infoAttach.getLatitude();
                        String Lng= infoAttach.getLongitude();


                        Intent i= new Intent(getActivity(), DetailActivity.class);
                        i.putExtra("title",mtitle);
                        i.putExtra("des",deskripsi);
                        i.putExtra("mharga",harga);
                        i.putExtra("image",UrlImage);
                        i.putExtra("lati",Lat);
                        i.putExtra("lngo",Lng);
                        startActivity(i);


                        Toast.makeText(getActivity(), "Info window clicked",
                                Toast.LENGTH_SHORT).show();
                    }
                });


//
//                mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
//                    @Override
//                    public boolean onMarkerClick(Marker marker) {
//
//                        String markerTitle = marker.getTitle();
//
//                        Intent i= new Intent(getActivity(), DetailActivity.class);
//                        i.putExtra("title",markerTitle);
//                        startActivity(i);
//                        return false;
//                    }
//                });

                if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)== PackageManager.PERMISSION_GRANTED){
                   buildGoogleApiClient();
                    mMap.setMyLocationEnabled(true);
                }
            }
        });

        return v;
    }


    protected synchronized void buildGoogleApiClient(){
        googleApiClient = new GoogleApiClient.Builder(getActivity())
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        googleApiClient.connect();
    }

    public boolean checkUserLocationPermission(){
        if(ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED ){
            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),Manifest.permission.ACCESS_FINE_LOCATION)){
                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, Request_User_Location_Code);
            }

            else {
                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, Request_User_Location_Code);
            }
            return false;
        }
        else {
            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode){
            case Request_User_Location_Code:
                if (grantResults.length > 0 && grantResults[0] ==PackageManager.PERMISSION_GRANTED){
                    if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)== PackageManager.PERMISSION_GRANTED){
                        if (googleApiClient == null){
                            buildGoogleApiClient();
                        }
                        mMap.setMyLocationEnabled(true);
                    }
                }
                else {
                    Toast.makeText(getActivity(), "Permission Denied...", Toast.LENGTH_SHORT).show();
                }
                return;
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        latitude=location.getLatitude();
        longitude= location.getLongitude();
    lastLocation=location;
    if (currentUserLocationMarker !=null){
        currentUserLocationMarker.remove();
    }

    LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
//        for (int i=0;i<arrayList.size();i++){
//            for(int j=0;j<title.size();j++){
//                for (int k=0;k<arrayList.size();k++){
//                    MarkerOptions markerOptions = new MarkerOptions();
//                    markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.icon_marker))
//                            .position(arrayList.get(i))
//                            .title(String.valueOf(title.get(j)))
//                            .snippet(String.valueOf(snipet.get(k)));
//                    InfoWindowData info = new InfoWindowData();
//                    info.setImage("ic_kost");
//                    info.setHarga("Rp.650.000");
//
//                    CustomInfoWindow customInfoWindow = new CustomInfoWindow(getActivity());
//                    mMap.setInfoWindowAdapter(customInfoWindow);
//                    Marker m = mMap.addMarker(markerOptions);
//                    m.setTag(info);
//                    m.showInfoWindow();
//                }
//                mMap.moveCamera(CameraUpdateFactory.newLatLng(arrayList.get(i)));
//            }
//
//        }


    MarkerOptions markerOptions= new MarkerOptions();
    markerOptions.position(latLng);
    markerOptions.title("User Current Location");
    markerOptions.snippet("Jalan tunjung tutur nomor 2x");
    markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
        InfoWindowData info = new InfoWindowData();
        info.setImage("ic_kost");
        info.setHarga("");

        CustomInfoWindow customInfoWindow = new CustomInfoWindow(getActivity());
        mMap.setInfoWindowAdapter(customInfoWindow);

    currentUserLocationMarker = mMap.addMarker(markerOptions);
    currentUserLocationMarker.setTag(info);
    currentUserLocationMarker.showInfoWindow();

    mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
    mMap.animateCamera(CameraUpdateFactory.zoomTo(12));




    if (googleApiClient !=null){
        LocationServices.FusedLocationApi.removeLocationUpdates(googleApiClient,this);

    }

    }


    @Override
    public void onConnected(@Nullable Bundle bundle) {
        locationRequest= new LocationRequest();
        locationRequest.setInterval(1100);
        locationRequest.setFastestInterval(1100);
        locationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);

        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)== PackageManager.PERMISSION_GRANTED)
        {
            LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, this);

        }

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

}

