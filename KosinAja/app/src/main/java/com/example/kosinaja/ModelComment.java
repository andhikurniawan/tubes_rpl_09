package com.example.kosinaja;

public class ModelComment {

    String id,email,comment;

    public ModelComment(String id, String email, String comment) {
        this.id = id;
        this.email = email;
        this.comment = comment;
    }

    public String getId() {
        return id;
    }

    public String getEmail() {
        return email;
    }

    public String getComment() {
        return comment;
    }
}
