package com.example.kosinaja;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;


public class pesankost1 extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    public String nama, noHP, pekerjaan, namaUniv, gender;

    EditText et_nama, et_noHP, et_pekerjaan, et_namaUniv;
    Button btn_lanjut;

    String[] genderArray = {"Laki-Laki", "Perempuan"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pesankost1);

        //declare xml components
        et_nama = findViewById(R.id.nama_input);
        et_noHP = findViewById(R.id.nohp_input);
        et_pekerjaan = findViewById(R.id.pekerjaan_input);
        et_namaUniv = findViewById(R.id.namauniv_input);
        btn_lanjut = findViewById(R.id.btn_next);
        Spinner spinner_gender = (Spinner) findViewById(R.id.spinner_gender);

        //put into string variables
        nama = et_nama.getText().toString();
        noHP = et_noHP.getText().toString();
        pekerjaan = et_pekerjaan.getText().toString();
        namaUniv = et_namaUniv.getText().toString();

        //gender spinner adapter
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, genderArray);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_gender.setAdapter(adapter);
        spinner_gender.setOnItemSelectedListener(this);

    }

    //get selected gender
    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        Spinner spinner_gender = (Spinner) findViewById(R.id.spinner_gender);
        gender = spinner_gender.getItemAtPosition(position).toString();
    }

    //if no gender selected
    @Override
    public void onNothingSelected(AdapterView<?> parent) {
        Toast.makeText(this, "Pilih salah satu jenis kelamin", Toast.LENGTH_SHORT).show();
    }

    public void lanjut(View view) {

        //validation
        if (TextUtils.isEmpty(et_nama.getText()) || TextUtils.isEmpty(et_noHP.getText()) || TextUtils.isEmpty(et_pekerjaan.getText())
        || TextUtils.isEmpty(et_namaUniv.getText())) {

            Toast.makeText(this, "Isi semua baris yang diperlukan", Toast.LENGTH_SHORT).show();

        } else {

            nama = et_nama.getText().toString();
            noHP = et_noHP.getText().toString();
            pekerjaan = et_pekerjaan.getText().toString();
            namaUniv = et_namaUniv.getText().toString();

            Intent intent = new Intent(getApplicationContext(),pesankost2.class);
            intent.putExtra("nama", nama);
            intent.putExtra("noHP", noHP);
            intent.putExtra("pekerjaan", pekerjaan);
            intent.putExtra("namaUniv", namaUniv);
            intent.putExtra("gender", gender);
            startActivity(intent);

        }
    }

}
