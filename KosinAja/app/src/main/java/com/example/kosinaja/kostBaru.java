package com.example.kosinaja;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class kostBaru extends Activity {
    Button tambahkost, bantuankost;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kost_baru);

        tambahkost = findViewById(R.id.button_kost);
        bantuankost = findViewById(R.id.bantuan_btn);

        tambahkost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent iptn = new Intent(getApplicationContext(),Input.class);
                startActivity(iptn);
            }
        });

        bantuankost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent btn = new Intent(getApplicationContext(),bantuanKost.class);
                startActivity(btn);
            }
        });

    }
}
