package com.example.kosinaja;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.FragmentActivity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.midtrans.sdk.corekit.callback.TransactionFinishedCallback;
import com.midtrans.sdk.corekit.core.LocalDataHandler;
import com.midtrans.sdk.corekit.core.MidtransSDK;
import com.midtrans.sdk.corekit.core.PaymentMethod;
import com.midtrans.sdk.corekit.core.TransactionRequest;
import com.midtrans.sdk.corekit.core.themes.CustomColorTheme;
import com.midtrans.sdk.corekit.models.CustomerDetails;
import com.midtrans.sdk.corekit.models.ItemDetails;
import com.midtrans.sdk.corekit.models.UserAddress;
import com.midtrans.sdk.corekit.models.UserDetail;
import com.midtrans.sdk.corekit.models.snap.CreditCard;
import com.midtrans.sdk.corekit.models.snap.TransactionResult;
import com.midtrans.sdk.uikit.SdkUIFlowBuilder;
import com.razorpay.Checkout;
import com.razorpay.PaymentResultListener;

import org.json.JSONObject;

import java.util.ArrayList;

public class DetailKost extends FragmentActivity implements OnMapReadyCallback {

    TextView mTitle, mGender, mDesc, mHarga, mLokasi, mAlamat, mStok, mWaktu;
    ImageView imageView;
    GoogleMap mMap;
    Button pay;
    private Marker markerKost;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_kost);


//        ActionBar actionBar = getSupportActionBar();
//        actionBar.setTitle("Detail Kost");
//        actionBar.setDisplayHomeAsUpEnabled(true);
//        actionBar.setDisplayShowHomeEnabled(true);

        SupportMapFragment mapFragmentS = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.mapS);
        mapFragmentS.getMapAsync(this);

        mTitle = findViewById(R.id.Title);
        mGender=findViewById(R.id.Gender);
        mDesc=findViewById(R.id.Description);
        mHarga=findViewById(R.id.Harga);
        mLokasi=findViewById(R.id.Lokasi);
        mAlamat=findViewById(R.id.Alamat);
        mStok=findViewById(R.id.Stok);
        imageView=findViewById(R.id.Image);
        mWaktu=findViewById(R.id.Waktu);

        //getData from intent
        byte [] bytes = getIntent().getByteArrayExtra("image");
        String title = getIntent().getStringExtra("judul");
        String description = getIntent().getStringExtra("description");
        String harga = getIntent().getStringExtra("harga");
        String stok = getIntent().getStringExtra("stok");
        String gender = getIntent().getStringExtra("gender");
        String alamat = getIntent().getStringExtra("alamat");
        String lokasi = getIntent().getStringExtra("lokasi");
        String waktu = getIntent().getStringExtra("waktu");
        Bitmap bmp = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);

        //set data to view
        mTitle.setText(title);
        mGender.setText(gender);
        mDesc.setText(description);
        mHarga.setText(harga);
        mLokasi.setText(lokasi);
        mAlamat.setText(alamat);
        mStok.setText(stok);
        mWaktu.setText(waktu);
        imageView.setImageBitmap(bmp);

        pay= (Button)findViewById(R.id.Btn_Detail);
        pay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(DetailKost.this, Pesan.class);
                startActivity(intent);
            }
        });

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        String latitude = getIntent().getStringExtra("lat");
        String longitude = getIntent().getStringExtra("lng");
        double lat=Double.parseDouble(latitude);
        double lng=Double.parseDouble(longitude);
        mMap= googleMap;
        mMap.getUiSettings().setZoomControlsEnabled(true);
                        mMap.getUiSettings().setMapToolbarEnabled(true);
        CameraPosition INIT = new CameraPosition.Builder().target(new LatLng(lat,lng)).zoom(15.5F).bearing(300F) // orientation
                .tilt(50F) // viewing angle
                .build();

        // use map to move camera into position
        mMap.moveCamera(CameraUpdateFactory.newCameraPosition(INIT));

        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(new LatLng(lat,lng));
        markerOptions.title(getIntent().getStringExtra("judul"));
        markerOptions.snippet(getIntent().getStringExtra("alamat"));
        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
        markerKost=mMap.addMarker(markerOptions);
//        mMap.moveCamera(CameraUpdateFactory.newLatLng(Data));
//        mMap.animateCamera(CameraUpdateFactory.zoomTo(12));



    }

//    @Override
//    public boolean onSupportNavigateUp() {
//        onBackPressed();
//        return true;
//    }


    }


